Traduction du livret xv6
========================

Vous trouverez ici :

  * la [traduction du livret xv6](https://pdagog.gitlab.io/xv6-livret/xv6-livret.pdf) sous forme d'un fichier PDF
  * les sources LaTeX dans le [dépôt Gitlab](https://gitlab.com/pdagog/xv6-livret)


Présentation de xv6
-------------------

[Xv6](https://github.com/mit-pdos/xv6-public)
est un système d'exploitation proche d'Unix
conçu à des fins pédagogiques et utilisé par le
MIT pour son
[cours de systèmes d'exploitation 6.828](https://pdos.csail.mit.edu/6.828).

Xv6 est accompagné d'un livret (en anglais) disponible sur
[https://github.com/mit-pdos/xv6-book](https://github.com/mit-pdos/xv6-book).


Traducteurs
-----------

Les traducteurs du livret sont Timothée Zerbib et Pierre David
