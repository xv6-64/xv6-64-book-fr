  400	struct q {
  401	  struct spinlock lock;
  402	  void *ptr;
  403	};
  404	
  405	void*
  406	send(struct q *q, void *p)
  407	{
  408	  acquire(&q->lock);
  409	  while(q->ptr != 0)
  410	    ;
  411	  q->ptr = p;
  412	  wakeup(q);
  413	  release(&q->lock);
  414	}
  415	
  416	void*
  417	recv(struct q *q)
  418	{
  419	  void *p;
  420	
  421	  acquire(&q->lock);
  422	  while((p = q->ptr) == 0)
  423	    sleep(q, &q->lock);
  424	  q->ptr = 0;
  425	  release(&q->lock);
  426	  return p;
  427	}
