    6	struct list *list = 0;
     	struct lock listlock;
    7	
    8	void
    9	insert(int data)
   10	{
   11	  struct list *l;
   12	  l = malloc(sizeof *l);
   13	  l->data = data;
   14	
     	  acquire(&listlock);
   15	  l->next = list;
   16	  list = l;
     	  release(&listlock);
   17	}
