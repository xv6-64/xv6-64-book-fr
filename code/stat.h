#define T_DIR  1   // Répertoire
#define T_FILE 2   // Fichier
#define T_DEV  3   // Périphérique

struct stat {
  short type;  // Type de fichier
  int dev;     // Numéro de périphérique du disque contenant le système de fichiers
  uint ino;    // Numéro inode
  short nlink; // Nombre de références vers ce fichier
  uint size;   // Taille du fichier en octets
};
