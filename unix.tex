\chapter {Interface du système d'exploitation}
  \label{chap:unix}

Le travail d’un système d’exploitation consiste à partager un ordinateur entre
plusieurs programmes et à fournir un ensemble de services plus utile
que celui fourni par le matériel seul.
Le système d'exploitation gère et abstrait la couche matérielle
de sorte que, par exemple, un logiciel de traitement de texte n'a pas
besoin de se préoccuper du type de disque dur utilisé.
Il partage également le matériel entre plusieurs programmes afin
qu'ils s'exécutent (ou semblent s'exécuter) en même temps.
Enfin, les systèmes d'exploitation fournissent aux programmes
des moyens contrôlés d'interagir ensemble, afin qu'ils puissent
partager des données ou travailler de concert.

Un système d'exploitation fournit des services aux programmes utilisateurs
via une interface. Concevoir une bonne interface s’avère être difficile.
D'une part, nous aimerions que l'interface soit simple et minimale, car
cela facilite la rédaction d'une implémentation correcte.
D'autre part, nous pourrions être tentés d’offrir de nombreuses
fonctionnalités sophistiquées aux applications.
Une bonne façon de résoudre ce dilemme est de concevoir des interfaces qui
reposent sur un nombre restreint de mécanismes qui peuvent être combinés
pour en offrir de plus généraux.

Ce livret utilise un système d'exploitation comme exemple concret pour
illustrer les concepts des systèmes d'exploitation. Ce système,
nommé xv6, fournit les interfaces de base introduites par le système
Unix de Ken~Thompson et Dennis~Ritchie, tout en imitant son design interne.
Unix fournit une interface minimale dont les mécanismes se combinent bien,
offrant un surprenant degré de généralité.
Cette interface a eu tellement de succès que les systèmes d'exploitation
modernes --- BSD, Linux, Mac OS X, Solaris, et même, dans une moindre mesure,
Microsoft Windows --- ont une interface de ce type.
Comprendre xv6 est un bon moyen de comprendre n'importe lequel de ces systèmes
et bien d'autres.

Comme le montre la figure~\ref{fig:os}, xv6 prend la forme traditionnelle d'un
«~noyau~», un programme particulier qui fournit des services aux autres
programmes en cours d'exécution.
Chaque programme en cours d'exécution, appelé «~processus~», possède son
espace mémoire contenant des instructions, des données et une pile.
Les instructions représentent les calculs à effectuer. Les données sont
les variables sur lesquelles les calculs vont être effectués. La pile organise
les appels de procédures du programme.

Lorsqu'un processus doit appeler un service du noyau, il demande un appel
de procédure à l'interface du système d'exploitation. Une telle procédure est
appelée un «~appel système
\ndtshort{Nous utiliserons les termes «~appel système~» et «~primitive système~»
(ou simplement primitive) indépendamment.}~».
L'appel système arrive au noyau~; le noyau effectue le service et retourne.
Ainsi, un processus alterne son exécution entre «~l'espace utilisateur~»
et «~l'espace noyau~».

Le noyau utilise les mécanismes matériels de protection du processeur
afin de s'assurer que chaque processus s'exécutant dans l'espace utilisateur
ne peut accéder qu'à son propre espace mémoire.
Le noyau s'exécute avec les privilèges matériels requis pour implémenter ces
protections tandis que les programmes utilisateurs s'exécutent sans
ces privilèges.
Quand un programme utilisateur fait un appel système, le matériel élève
les privilèges et exécute une fonction pré-définie dans le noyau.


\begin {figure}[t]
  \begin {center}
    \includegraphics [width=10cm] {\figpath{os.pdf}}
  \end {center}
  \caption {Un noyau et deux processus utilisateurs.}
  \label{fig:os}
\end {figure}


La liste des appels système qu'un noyau fournit correspond à l'interface
que voient les programmes utilisateurs.
Le noyau xv6 fournit une partie des services et appels système qu'offrent 
traditionnellement les noyaux Unix.
La figure~\ref{fig:api} liste tous les appels système de xv6.

La suite de ce chapitre décrit brièvement les services fournis par xv6
---~processus, mémoire, descripteurs de fichier, tubes et
système de fichiers~--- et les illustre avec des extraits de code et 
des discussions sur la façon dont le \emph{shell}, qui est l'interface
utilisateur principale dans les systèmes de type Unix traditionnels,
les utilise.
L'utililisation des appels système par le shell illustre le soin
avec lequel ils ont été conçus.

Le shell est un programme ordinaire qui lit des commandes entrées par
l'utilisateur et les exécute.
Le fait que le shell soit un programme utilisateur et non une partie du noyau
illustre la puissance de l'interface des appels système~:
le shell est un programme qui n'a rien de particulier.
Cela signifie également que le shell est facilement remplaçable~;
par conséquent, les systèmes Unix modernes possèdent une large variété
de shells, chacun ayant sa propre interface utilisateur et ses
propres fonctionnalités.
Le shell de xv6 est une implémentation simplifiée de ce qui fait 
le cœur du shell de Bourne d'Unix
\ndtshort{Le shell de Bourne est le shell par défaut depuis la version 7
d'Unix et a servi de base pour la norme POSIX.
Son fichier exécutable se trouve à l'emplacement \chemin{/bin/sh}.}.
Son implémentation peut être trouvée à la ligne \xvline{sh.c:/^// Shell/}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processus et mémoire
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Processus et mémoire}
  \label{sec:processus_et_memoire}

Un processus xv6 est constitué d'un espace mémoire utilisateur (instructions,
données et pile), et d'un état par processus conservé par le noyau
\ndtshort{Pour chaque processus, le noyau sauvegarde ses attributs
(PID, PPID, UID, GID...) ainsi que les registres processeurs permettant
de restaurer son contexte (cf. Chapitre \ref{}).}.
Xv6 peut partager le temps (\emph{time sharing}) entre les processus~:
il commute de manière transparente les processus en attente d'exécution sur
les processeurs disponibles.
Lorsqu'un processus n'est pas en cours d'exécution, xv6 sauvegarde ses registres
processeurs afin de les restaurer lors de sa prochaine exécution.
Le noyau associe à chaque processus un identifiant, ou \codestyle{pid}.

\begin {figure}[t]
  \begin {center}
    \small
    \begin{tabular}{|l|l|}
      \hline
      \textbf{Appel système} & \textbf{Description} \\
      \hline
      \xvfarg{fork}{} &	Crée un processus \\
      \hline
      \xvfarg{exit}{} &	Termine le processus courant \\
      \hline
      \xvfarg{wait}{} &	Attend qu'un processus enfant se termine \\
      \hline
      \xvfarg{kill}{pid} &	Termine le processus \codestyle{pid} \\
      \hline
      \xvfarg{getpid}{} &	Retourne le pid du processus courant \\
      \hline
      \xvfarg{sleep}{n} &	Met le processus en attente pendant
                          \codestyle{n} tops d'horloge \\
      \hline
      \xvfarg{exec}{filename, *argv} &	Charge un fichier et l'exécute \\
      \hline
      \xvfarg{sbrk}{n}	& Augmente l'espace mémoire du processus de
                          \codestyle{n}~octets \\
      \hline
      \xvfarg{open}{filename, flags} &	Ouvre un fichier~; \codestyle{flags}
                                        indique le mode (lecure/écriture) \\
      \hline
      \xvfarg{read}{fd, buf, n} &	Stocke \codestyle{n}~octets depuis
                                  un fichier ouvert dans \codestyle{buf} \\
      \hline
      \xvfarg{write}{fd, buf, n} &	Écrit \codestyle{n}~octets dans
                                    un fichier ouvert \\
      \hline
      \xvfarg{close}{fd}	& Libère le descripteur de fichier \codestyle{fd} \\
      \hline
      \xvfarg{dup}{fd}	& Duplique le descripteur de fichier \codestyle{fd} \\
      \hline
      \xvfarg{pipe}{p}	& Crée un tube et retourne les descripteurs
                          de fichier dans \codestyle{p} \\
      \hline
      \xvfarg{chdir}{dirname} & Change le répertoire courant \\
      \hline
      \xvfarg{mkdir}{dirname} &	Crée un nouveau répertoire \\
      \hline
      \xvfarg{mknod}{name, major, minor} &	Crée un fichier spécial
                                            de type périphérique \\
      \hline
      \xvfarg{fstat}{fd} &	Renvoie les informations sur un fichier ouvert \\
      \hline
      \xvfarg{link}{f1, f2} &	Crée un nouveau nom (\codestyle{f2})
                              pour le fichier \codestyle{f1} \\
      \hline
      \xvfarg{unlink}{filename} &	Supprime un fichier \\
      \hline
    \end{tabular}
  \end {center}
  \caption {Liste des appels système de xv6.}
  \label{fig:api}
\end {figure}

Un processus peut créer un autre processus en utilisant l'appel système
\xvf{fork}. \xvf{Fork} crée un nouveau processus, appelé
«~processus enfant~», avec exactement le même contenu mémoire que le
processus original, appelé «~processus parent~». \xvf{Fork} retourne 
à la fois dans le parent et dans l'enfant.
Dans le parent, \xvf{fork} renvoie le pid du processus enfant~;
dans l'enfant, il renvoie zéro. Par exemple, considérons le fragment
de programme suivant~:
\code{ex_fork.c}
L'appel système \xvf{exit} provoque l'arrêt du processus
appelant et la libération des ressources comme la mémoire et les
fichiers ouverts.
\label{sec:unix:wait}
L'appel système \xvf{wait} renvoie le pid d'un enfant du processus courant
terminé~; si aucun des enfants de l'appelant n'est terminé, \xvf{wait} attend
qu'un d'eux se termine.
Dans l'exemple, les sorties~:
\begin{quote}
  \codestyle{parent: child: 1234}

  \codestyle{child: exiting}
\end{quote}
peuvent être affichées dans cet ordre ou son inverse, dépendant de si c'est le
parent ou l'enfant qui appelle le premier sa fonction \xvf{printf}.
Après que l'enfant ait terminé, le \xvf{wait} du parent retourne, provoquant
l'affichage par le \xvf{printf} du parent~:
\begin{quote}
  \codestyle{parent: child 1234 is done}
\end{quote}
Bien qu'initialement, l'enfant possède le même contenu mémoire que le parent,
le parent et l'enfant s'exécutent avec des espaces mémoires et des
registres différents~: changer une variable dans l'un n'affecte pas l'autre.
Par exemple, lorsque la valeur de retour de \xvf{wait} est stockée dans 
la variable \codestyle{pid} dans le processus parent, cela ne change pas
la valeur de la variable \codestyle{pid} dans l'enfant.
La valeur de \codestyle{pid} dans l'enfant sera toujours zéro.

L'appel système \xvf{exec} remplace l'espace mémoire du processus appelant
par un nouvel espace mémoire chargé depuis l'image d'un fichier stocké dans 
le système de fichiers. Ce fichier doit avoir un format particulier,
qui spécifie quelle partie du fichier comporte les instructions,
quelle partie comporte les données, à quelle instruction commencer, etc.
Xv6 utilise le format ELF, que le chapitre \ref{chap:mem}
détaille davantage.
Lorsque \xvf{exec} termine avec succès, il ne retourne pas
au programme appelant~; à la place, les instructions chargées depuis
le fichier commencent leur exécution à partir du point d'entrée déclaré
dans l'en-tête ELF.
\xvf{Exec} prend deux arguments~: le nom du fichier contenant l'exécutable
et un tableau d'arguments sous forme de chaînes de caractères.
Par exemple~:
\code{ex_exec.c}
Ce fragment remplace le programme appelant par une instance du programme
\chemin{/bin/echo} s'exécutant avec la liste d'arguments \codestyle{echo hello}.
La plupart des programmes ignorent le premier argument, qui est par convention
le nom du programme.

Le shell de xv6 utilise les appels ci-dessus pour exécuter des programmes
pour le compte des utilisateurs. La structure principale du shell est simple
(cf. \xvfct{main}{sh.c:/^main/}).
La boucle principale lit une ligne entrée par l'utilisateur avec
\xvf{getcmd}. Puis, elle appelle \xvf{fork}, qui crée une copie
du processus du shell. Le parent appelle \xvf{wait}, tandis que l'enfant
exécute la commande. Par exemple, si l'utilisateur avait tapé 
\codestyle{"echo hello"}, \xvf{runcmd} aurait été appelé avec
\codestyle{"echo hello"} pour argument.
\xvfct{runcmd}{sh.c:/^runcmd/} exécute la commande réelle.
Pour \codestyle{"echo hello"} il appellerait 
\xvfct{exec}{sh.c:/exec.ecmd/}.
Si \xvf{exec} réussit, alors l'enfant exécutera les instructions de
\codestyle{echo} au lieu de \xvf{runcmd}.
À un moment, \codestyle{echo} va appeler \xvf{exit}, ce qui provoquera la 
terminaison de la fonction \xvf{wait} du parent dans la fonction 
\xvfct{main}{sh.c:/^main/}.
Vous pourriez vous demander pourquoi \xvf{fork} et \xvf{exec} ne sont
pas combinés en un appel unique~; nous allons voir plus loin qu'il est plus
astucieux de séparer les appels pour la création d'un processus
et le chargement d'un programme.

Xv6 alloue la majorité de son espace mémoire utilisateur de façon implicite~:
\xvf{fork} alloue la mémoire requise pour permettre la copie de la mémoire
du processus parent, et \xvf{exec} alloue suffisamment de mémoire pour
contenir le fichier exécutable.
Un processus qui nécessite plus de mémoire durant son exécution (peut-être
à cause d'un \xvf{malloc}) peut appeler \xvfarg{sbrk}{n} pour augmenter
de $n$~octets la partie de la mémoire consacrée aux données~;
\xvf{sbrk} renvoie l'adresse de la mémoire nouvellement allouée.

Xv6 ne fournit pas de notion d'utilisateur ni de protection
entre utilisateurs~; en termes Unix, tous les processus xv6 s'exécutent en tant
que superutilisateur (\emph{root}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% I/O et Descripteurs de fichier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {I/O et Descripteurs de fichier}
  \label{sec:io_et_descripteurs_de_fichier}

Un «~descripteur de fichier~» est un petit entier représentant un objet
géré par le noyau duquel un processus peut lire ou sur lequel il peut écrire.
Un processus peut obtenir un descripteur de fichier en ouvrant un fichier,
un répertoire ou un périphérique, ou bien en créant un tube, ou en dupliquant
un autre descripteur de fichier.
Pour simplifier, nous allons souvent désigner l'objet auquel se réfère
un descripteur de fichier par le mot fichier~; l'interface de descripteur
de fichier abstrait les différences entre fichiers, tubes et périphériques,
les faisant tous paraître comme des flux d'octets.

En interne, le noyau xv6 utilise le descripteur de fichier comme un index
dans une table propre à chaque processus, de sorte que chacun ait
un espace privé de descripteurs de fichier commençant à zéro.
Par convention, un processus lit depuis le descripteur de fichier 0
(entrée standard), écrit ses sorties sur le descripteur de fichier 1
(sortie standard) et écrit ses messages d'erreur sur le descripteur de fichier 2
(sortie d'erreur).
Comme nous le verrons, le shell exploite cette convention pour implémenter
les redirections des entrées/sorties et les pipelines. Le shell s'assure 
de toujours posséder au moins trois descripteurs de fichier ouvert
[\xvline{sh.c:/open..console/}], qui sont par défaut les descripteurs de fichier
de la console.

Les appels système \xvf{read} et \xvf{write} lisent et écrivent des octets
sur les fichiers ouverts désignés par les descripteurs de fichier.
L'appel \xvfarg{read}{fd, buf, n} lit au plus \codestyle{n}~octets
depuis le descripteur \codestyle{fd}, les copie dans \codestyle{buf} et renvoie
le nombre d'octets lus.
Chaque descripteur de fichier qui fait référence à un fichier lui associe
un \emph{offset}\ndtshort{Le terme \emph{offset} correspond à «~position
relative~» (par rapport à une référence). Une traduction pourrait être
«~décalage~» mais nous avons préféré conserver le terme anglais, faute
de terme communément admis. Dans ce contexte, un \emph{offset} est donc
le nombre d'octets depuis le début du fichier.}.
\xvf{Read} lit des données à partir de l'offset associé au fichier puis
augmente cet offset du nombre d'octets lus~: un \xvf{read} ultérieur
va retourner les octets suivant ceux retournés par le premier \xvf{read}.
Lorsqu'il n'y a plus d'octet à lire, \xvf{read} renvoie zéro pour signaler
la fin du fichier.

L'appel \xvfarg{write}{fd, buf, n} écrit \codestyle{n}~octets depuis
\codestyle{buf} sur le descripteur de fichier \codestyle{fd} et renvoie
le nombre d'octets écrits.
Ce nombre n'est inférieur à \codestyle{n} que lorsqu'il se produit une erreur.
Tout comme \xvf{read}, \xvf{write} écrit des données à l'offset
courant du fichier puis augmente cet offset du nombre
d'octets écrits~: chaque \xvf{write} reprend là où le précédent
s'était arrêté.

Le fragment de programme suivant (formant le cœur de \codestyle{cat}) copie
des données depuis l'entrée standard sur la sortie standard. Si une erreur
survient, il écrit un message sur la sortie d'erreur.
\code{ex_cat.c}

Ce qu'il faut retenir de ce morceau de code est que \codestyle{cat} ne sait pas
s'il est en train de lire un fichier, la console ou un tube.
De même, \codestyle{cat} ne sait pas s'il écrit sur la console, un fichier 
ou n'importe quoi d'autre.
L'utilisation de descripteurs de fichier et la convention que le descripteur
de fichier 0 correspond à l'entrée et le descripteur 1 à la sortie conduisent à
une implémentation simple de \codestyle{cat}.

L'appel système \xvf{close} libère un descripteur de fichier, le rendant
réutilisable par un futur appel à \xvf{open}, \xvf{pipe} ou \xvf{dup}
(voir ci-après).
Un descripteur de fichier nouvellement alloué correspond toujours au plus petit
descripteur non utilisé par le processus courant.

Les descripteurs de fichier couplés à \xvf{fork} simplifient l'implémentation
des redirections d'entrées/sorties.
\xvf{Fork} copie la table des descripteurs de fichier du parent dans
sa mémoire, de sorte que l'enfant démarre avec exactement les mêmes
fichiers ouverts que le parent.
L'appel système \xvf{exec} remplace le processus appelant en mémoire, mais
conserve sa table des fichiers ouverts.
Ce comportement permet au shell d'implémenter les redirections
d'entrées/sorties en appelant \xvf{fork}, en réouvrant le descripteur de fichier
choisi puis en exécutant le nouveau programme.
Voici une version simplifiée du code exécuté par un shell pour
la commande \codestyle{cat < input.txt}
\code{ex_cat_2.c}

Après la fermeture par l'enfant du descripteur 0, il est garanti 
qu'\xvf{open} utilise ce descripteur pour le fichier \codestyle{input.txt}
nouvellement ouvert~: 0 sera le plus petit descripteur de fichier disponible.
\codestyle{Cat} s'exécutera alors avec son descripteur
de fichier 0 (entrée standard) faisant référence à \codestyle{input.txt}.

Le code pour les redirections d'entrées/sorties dans le shell de xv6
fonctionne exactement de cette façon 
[\xvline{sh.c:/case.REDIR/}].
Rappelez-vous qu'à ce moment dans le code, le shell a déjà forké
le shell enfant et que \xvf{runcmd} appellera \xvf{exec} pour charger
le nouveau programme. La raison de la séparation de \xvf{fork}
et \xvf{exec} devrait maintenant être claire, car s'ils sont séparés,
le shell peut créer un enfant avec \xvf{fork}, utiliser \xvf{open},
\xvf{close} et \xvf{dup} dans l'enfant pour changer les descripteurs
de fichier de l'entrée et de la sortie standard, avant
de lancer \xvf{exec}. Aucun changement dans le programme appelé
par \xvf{exec} (\codestyle{cat} dans notre exemple) n'est requis.
Si \xvf{fork} et \xvf{exec} étaient combinés en un seul appel système,
un autre schéma (probablement plus complexe) aurait été nécessaire pour pemettre
au shell de rediriger l'entrée et la sortie standard, ou alors
le programme lui-même devrait comprendre comment rediriger l'entrée/sortie.

Bien que \xvf{fork} copie la table des descripteurs de fichier, les offsets sont
partagés entre parent et enfant.
Considérons l'exemple~:
\code{ex_offset.c}
À la fin de ce morceau de code, le fichier attaché au descripteur 1 contiendra
\codestyle{hello world}.
Le \xvf{write} du parent (qui, grâce à \xvf{wait}, ne s'exécute qu'après
la terminaison de l'enfant) reprend là où le \xvf{write} de l'enfant s'était
arrêté.
Ce comportement permet de produire une sortie séquentielle à partir
de séquences de commandes shell, comme
\codestyle{(echo hello; echo world) > output.txt}.

L'appel système \xvf{dup} duplique un descripteur de fichier existant,
et renvoie un nouveau descripteur faisant référence au même objet
d'entrée/sortie sous-jacent. Les deux descripteurs de fichier partagent
le même offset, comme le font les descripteurs de fichier dupliqués
par \xvf{fork}.
Voici une autre façon d'écrire \codestyle{hello world} dans un fichier~:
\code{ex_dup.c}

Deux descripteurs de fichier partagent leur offset s'ils dérivent du même
descripteur par une séquence d'appels à \xvf{fork} et \xvf{dup}.
Hormis cela, les descripteurs de fichier ne partagent pas leur offset,
même s'ils résultent d'appels à \xvf{open} sur le même fichier.
\xvf{Dup} permet aux shells d'implémenter des commandes comme~:
\codestyle{ls fichier\_existant fichier\_inexistant > tmp1 2>\&1}.
Le «~\codestyle{2>\&1}~» indique au shell de transmettre à la commande
un descripteur de fichier 2 qui est une copie (via \xvf{dup}) du descripteur 1.
Le nom du fichier existant et le message d'erreur pour le fichier non existant
vont tous deux être écrits dans le fichier \codestyle{tmp1}.
Le shell de xv6 ne supporte pas les redirections d'entrées/sorties pour
le descripteur correspondant à la sortie d'erreur, mais vous savez maintenant
comment l'implémenter.

Les descripteurs de fichier constituent une puissante abstraction
car ils masquent les détails de ce à quoi ils sont connectés~:
un processus écrivant sur le descripteur de fichier 1 peut écrire sur
un fichier, un périphérique tel que la console ou un tube.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tubes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Tubes}
  \label{sec:tubes}

Un \emph{tube} est un petit espace noyau se présentant aux processus comme
une paire de descripteurs de fichier, un pour la lecture et l'autre
pour l'écriture.
Écrire des données à un bout du tube rend ces données accessibles à la lecture
à l'autre bout du tube.
Les tubes fournissent un moyen aux processus de communiquer.

L'exemple de code suivant exécute le programme \codestyle{wc} avec
l'entrée standard connectée au côté lecture du tube.
\code{ex_tube.c}
Le programme appelle \xvf{pipe}, qui crée un nouveau tube et sauvegarde
les descripteurs de fichier pour lecture et écriture dans le tableau
\codestyle{p}.
Après \xvf{fork}, le parent comme l'enfant auront leurs descripteurs de fichier
faisant référence au tube.
Le processus enfant duplique l'extrémité lecture sur son descripteur 0,
ferme les descripteurs de fichier de \codestyle{p} et exécute \codestyle{wc}.
Lorsque \codestyle{wc} lit depuis son entrée standard, il lit depuis le tube.
Le processus parent ferme l'extrémité lecture du tube, écrit sur le tube
puis ferme l'extrémité écriture du tube.

Si aucune donnée n'est disponible, un appel à \xvf{read} sur un tube résulte en
l'attente qu'une donnée soit écrite ou que tous les descripteurs de fichier
faisant référence à l'extrémité écriture du tube soient fermés~; dans ce dernier
cas, \xvf{read} renvoie 0, exactement comme s'il avait atteint la fin
d'un fichier de données.
Le fait que \xvf{read} soit bloquant \{jusqu'à ce qu'il soit impossible que
de nouvelles données arrivent / tant que de nouvelles données peuvent arriver\}
est une des raisons pour lesquelles il est important que le processus enfant
ferme l'extrémité écriture du tube avant d'exécuter \codestyle{wc}~:
si l'un des descripteurs de fichier de
\codestyle{wc} faisait référence à l'extrémité écriture du tube,
\codestyle{wc} ne percevrait jamais la fin du fichier.

Le shell de xv6 implémente les pipelines tels que
\codestyle{grep fork sh.c | wc -l} de manière similaire au code ci-dessus
\xvline{sh.c:/case.PIPE/}.
Le processus enfant crée un tube pour connecter la partie gauche du pipeline
à sa partie droite. Puis il appelle \xvf{fork} et \xvf{runcmd} pour la partie
gauche du pipeline et \xvf{fork} et \xvf{runcmd} pour la partie droite,
et attend que les deux se terminent.
La partie droite du pipeline peut très bien être une commande qui contient
elle-même un tube (par exemple \codestyle{a | b | c}), qui lui-même fork
deux enfants (un pour \codestyle{b} et l'autre pour \codestyle{c}).
Ainsi, le shell peut créer un arbre de processus. Les feuilles de cet arbre
sont les commandes et les noeuds internes les processus qui attendent que
les enfants gauche et droit retournent. En principe, on pourrait faire
en sorte que les nœuds intérieurs exécutent l’extrémité gauche du pipeline,
mais le réaliser correctement risquerait de compliquer la mise en œuvre.

Les tubes peuvent ne pas sembler plus puissants que les fichiers temporaires~:
le pipeline \codestyle{hello world | wc} peut être implémenté sans tubes
de la manière suivante
\codestyle{echo hello world > /tmp/xyz; wc < /tmp/xyz}
Les tubes ont au minimum quatre avantages sur les fichiers temporaires
dans cette situation.
D'abord, les tubes se suppriment automatiquement~; avec une redirection passant
par des fichiers, le shell devrait faire attention lors de la suppression de
\chemin{/tmp/xyz} une fois ses tâches terminées.
Ensuite, les tubes peuvent échanger des flux de données arbitrairement grands
alors que la redirection par fichier nécessite une place suffisante sur
le disque pour stocker toutes les données.
De plus, les tubes permettent une exécution parallèle des différentes étapes
du pipeline, alors que l'approche par fichier requiert la terminaison
du premier programme avant de démarrer le second.
Enfin, si vous implémentez une communication entre processus, le bloquage
des tubes en lecture/écriture est plus efficace que la sémantique
non bloquante des fichiers.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Système de fichiers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Système de fichiers}
  \label{sec:systeme_de_fichiers}

Le système de fichiers de xv6 fournit des fichiers de données, représentés par
une suite linéaire d'octets, et des répertoires, qui contiennent
des références nommées à des fichiers de données et à d'autres répertoires.
Les répertoires forment un arbre commençant à un répertoire particulier
appelé la «~racine~».
Un «~chemin~» tel que \chemin{/a/b/c} fait référence au fichier ou répertoire
nommé \chemin{c} situé à l'intérieur du répertoire nommé \chemin{b}, lui-même
situé à l'intérieur du répertoire nommé \chemin{a}, lui-même situé
dans le répertoire racine \chemin{/}.
Les chemins qui ne commencent pas par \chemin{/} sont évalués relativement
au «~répertoire courant~» du processus appelant, qui peut être modifié
à l'aide de l'appel système \xvf{chdir}.
Les deux fragments de code suivants ouvrent le même fichier (en supposant
que tous les répertoires impliqués existent)~:
\code{ex_chemin_relatif.c}
\code{ex_chemin_absolu.c}
La première partie du code change le répertoire courant du processus
pour /a/b~; la seconde partie ne fait pas référence au répertoire courant
du processus, pas plus qu'elle ne le modifie.

Il existe plusieurs appels système qui créent un nouveau fichier
ou répertoire~: \xvf{mkdir} qui crée un nouveau répertoire,
\xvf{open} avec l'option \codestyle{O\_CREATE} qui crée un nouveau fichier
de données et \xvf{mknod} qui crée un nouveau fichier de type périphérique.
L'exemple suivant les illustre tous les trois~:
\code{ex_creation_fichier.c}
\xvf{Mknod} crée un fichier dans le système de fichiers, mais ce fichier
est vide. Les métadonnées de ce fichier le définissent comme un fichier spécial
de type périphérique et conservent ses numéros de périphérique majeur et mineur
\ndtshort{Le numéro majeur correspond au numéro du pilote.
Le numéro mineur correspond au numéro du périphérique géré par ce pilote.}
(représentés par les deux arguments de \xvf{mknod}) qui identifient
de manière unique un périphérique du noyau.
Lorsqu'un processus ouvrira ce fichier, le noyau transférera les appels système
\xvf{read} et \xvf{write} à l'implémentation du noyau du périphérique au lieu
de le faire parvenir au système de fichiers.

\xvf{Fstat} récupère les informations à propos de l'objet auquel se réfère
un descripteur de fichier. Cette fonction remplit une structure de données
\codestyle{struct stat} définie dans \fichier{stat.h} par~:
\code{stat.h}

Il faut distinguer le nom des fichiers des fichiers eux-mêmes~;
le même fichier sous-jacent, appelé «~inode
\ndtshort{Le mot inode vient de la contraction des mots index et node, c'est
pourquoi il sera par la suite considéré comme étant masculin.}
~», peut avoir plusieurs noms,
appelés «~liens~». L'appel système \xvf{link} crée un autre nom dans
le système de fichiers, faisant référence au même inode qu'un fichier
pré-existant. Ce morceau de code crée un nouveau fichier nommé à la fois
\codestyle{a} et \codestyle{b}~:
\code{ex_link.c}
Lire ou écrire sur a revient à lire ou écrire sur b.
Chaque inode est référencé par un unique «~numéro d'inode~».
Après l'exécution du morceau de code ci-dessus, il est possible de déterminer
que \codestyle{a} et \codestyle{b} font référence au même fichier en utilisant
\xvf{fstat}~: tous deux vont avoir le même numéro d'inode (\codestyle{ino}),
et le compteur de références du fichier (\codestyle{nlink}) aura pour valeur 2.

L'appel système \xvf{unlink} supprime un nom du système de fichiers.
Le numéro d'inode et l'espace mémoire contenant les données du fichier
ne seront libérés que lorsque le compteur de référence vaudra 0 et qu'aucun
descripteur de fichier n'y fera plus référence.
Ainsi, ajouter \xvfarg{unlink}{"a"} au bloc de code précédent laisse accessible
l'inode et le contenu du fichier via \codestyle{b}. De plus,
\code{ex_temp_inode.c}
est une manière idiomatique de créer un inode temporaire qui sera supprimé
lorsque le processus fermera le descripteur de fichier \codestyle{fd}
ou lorsqu'il se terminera.

Les commandes shell pour utiliser les opérations du système de fichiers sont
implémentées en tant que programmes utilisateurs comme \xvf{mkdir}, \xvf{ln},
\xvf{rm}, etc.
Une telle conception permet à n'importe qui d'étendre le shell avec de nouvelles
commandes utilisateurs en ajoutant simplement un nouveau programme
en mode non privilégié.
Avec le recul, cette façon de faire semble évidente, mais d'autres systèmes
conçus à la même époque qu'Unix incorporaient souvent de telles commandes dans
le shell (et ont incorporé le shell dans le noyau).

\xvf{Cd} est une exception puisqu'il est lié au shell 
[\xvline{sh.c:/if.buf.0..==..c/}].
\xvf{Cd} doit changer le répertoire de travail courant du shell lui-même.
Si \xvf{cd} était exécuté comme une commande ordinaire, le shell aurait forké
un processus fils, ce fils aurait exécuté \xvf{cd} et \xvf{cd} aurait changé
le répertoire de travail courant du \textbf{fils}. Le répertoire de travail
courant du parent (c.à.d le répertoire de travail courant du shell)
n'aurait pas changé.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dans la réalité
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Dans la réalité}
  \label{sec:dans_la_réalite_unix}

L'association par Unix des descripteurs de fichiers «~standards~», des tuyaux
ainsi que de la syntaxe pratique du shell pour exécuter des opérations
sur ces derniers a été une avancée majeure dans la rédaction de programmes 
réutilisables dans un cadre général.
L'idée a suscité toute une culture d'«~outils logiciel~», en grande partie
responsable de la puissance et de la popularité d'Unix,
et le shell était le premier soi-disant «~langage de script~».
L’interface des appels système Unix persiste aujourd’hui dans des systèmes
tels que BSD, Linux et Mac OS X.

L'interface des appels système Unix a été standardisée à travers le standard
POSIX (\emph{Portable Operating System Interface}).
Xv6 n'est \textbf{pas} conforme à POSIX.
Il n'implémente pas certains appels système (dont certains très basiques comme
\xvf{lseek}), il n'implémente des appels système que partiellement, etc.
Nos principaux objectifs pour xv6 sont la clarté et la simplicité
en fournissant une interface des appels système semblable à celle d'Unix.
De nombreuses personnes ont étendu xv6 en ajoutant quelques
appels système basiques et une bibliothèque C simple de sorte qu'il puisse
exécuter des programmes Unix basiques.
Les noyaux modernes fournissent toutefois de nombreux appels système
supplémentaires, et beaucoup plus de types de services qu'xv6.
Par exemple, ils supportent la mise en réseau, un système de fenêtrage,
des \emph{threads}
\ndtshort{Il n'existe pas vraiment de traduction française du mot \emph{thread}.
Bien que certains lui préfèrent «~fil d'exécution~», nous conserverons dans ce
document le mot d'origine.}
au niveau utilisateur, des pilotes pour de nombreux périphériques...
Les noyaux modernes évoluent continuellement et rapidement, et offrent
de nombreuses fonctionnalités en plus de POSIX.

Les systèmes d'exploitation modernes dérivant d'Unix n'ont pour la plupart
pas suivi le modèle Unix de représentation des périphériques en tant que
fichiers spéciaux, comme la \codestyle{console} dont nous avons discuté
plus tôt.
Les auteurs d'Unix sont allés plus loin en créant Plan 9
\ndtshort{Plan 9 est un système d'exploitation créé pour régler certains
problèmes d'Unix jugés trop profonds pour être corrigés.
Plan 9 hérite de notions Unix et en améliore certaines comme
l'adage du «~tout fichier~».},
qui applique le concept du «~tout fichier~» aux systèmes modernes,
représentant les réseaux, les graphiques et les autres ressources comme
des fichiers ou des arborescences de fichiers.

L'abstraction du système de fichiers a été une puissante idée.
Cependant, il existe d'autres modèles pour les interfaces des systèmes
d'exploitation.
Multics, un prédecesseur d'Unix, abstrait le stockage des fichiers de façon
à le faire ressembler à de la mémoire, produisant une interface très différente.
La complexité du design de Multics a eu une influence directe sur
les concepteurs d'Unix, qui ont tenté de créer quelque chose de plus simple.

% \ndtlong
% {
%   Ce paragraphe a été supprimé de la version originale du livret,
%   je le replace ici à titre informatif.
% 
%   Une interface de système d'exploitation qui est passé de mode il y a
%   plusieurs décennies mais qui commence à revenir est l'idée d'un moniteur
%   de machine virtuelle.
%   De tels systèmes fournissent une interface en apparence différente de xv6,
%   mais les concepts de base restent les mêmes~:
%   Une machine virtuelle, comme un processus, consiste en un espace mémoire et
%   un ou plusieurs ensembles de registres~;
%   la machine virtuelle a accès à un fichier volumineux appelé disque virtuel
%   au lieu d'un système de fichier~;
%   les machines virtuelles s'echangent des messages entre-elles et avec
%   le monde extérieur en utilisant des périphériques réseaux virtuels au lieu
%   de tubes et de fichiers.
% }

Ce livret étudie comment xv6 implémente son interface de type Unix, mais 
ces idées et concepts s'appliquent bien au delà d'Unix.
Tous les systèmes d'exploitation doivent multiplexer les processus sur
le matériel \{sous-jacent/impliqué\}, isoler les processus les uns des autres
et fournir des mécanismes permettant une communication entre processus.
Après avoir étudié xv6, vous devriez être capable d'examiner d'autres
systèmes d'exploitation plus complexes et d'identifier les concepts de xv6
également présents dans ces systèmes.
