\chapter {Le matériel du PC}
  \label{app:hw}

Cette annexe décrit le matériel des ordinateurs personnels (PC),
plateforme sur laquelle tourne xv6.

Un PC est un ordinateur conforme à plusieurs normes industrielles,
avec pour objectif qu'un logiciel donné puisse s'exécuter sur des machines
vendues par différents fournisseurs.
Ces normes évoluent avec le temps et un PC des années 90 ne ressemble pas
aux PC modernes.
La plupart des normes actuelles sont publiques et leur documentation
est disponible en ligne.

Vu de l'extérieur, un PC est une boîte avec un clavier, un écran et différents
périphériques (par exemple les CD-ROM, etc.).
À l'intérieur de cette boîte se trouve un circuit imprimé (la «~carte mère~»)
avec des puces de CPU, de mémoire, graphiques, de controlleur d'entrées/sorties
et des bus grâce auxquels les puces communiquent.
Les bus respectent des protocols standards (comme par exemple PCI et USB)
de sorte que les périphériques fonctionnent avec des PC de différents vendeurs.

De notre point de vue, nous pouvons abstraire les PC en trois composants~:
le processeur, la mémoire et les périphériques d'entrées/sorties.
Le processeur effectue des calculs, la mémoire contient des instructions et
des données pour ces calculs et les périphériques permettent au processeur
d'intéragir avec le matériel pour le stockage, la communication et
d'autres fonctions.

On peut représenter la mémoire principale comme étant connectée au processeur
par un ensemble de fils ou de liens, certains pour les bits d'adresse,
certains pour les bits de données et d'autres pour les indicateurs
\ndtshort{Originallement \emph{flags} dans le texte, ce mot sera remplaçé
par indicateur ou bits.}
de contrôle.
Pour lire une valeur depuis la mémoire principale, le processeur envoie
des tensions hautes ou basses représentant des bits à 1 ou à 0 sur le lien
des adresses et un 1 sur le lien «~lecture~» pendant une durée déterminée, puis
lit les valeurs renvoyées en interprétant les tensions sur le lien des données.
Pour écrire une valeur dans la mémoire principale, le processeur envoie
les bits appropriés sur les liens adresse et données et un 1 sur
le lien «~écriture~» durant une durée déterminée.
Les interfaces mémoires sont en réalité plus complexes, mais les détails de leur
fonctionnement ne sont importants que si l'on doit atteindre
de hautes performances.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processeur et mémoire
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Processeur et mémoire}
  \label{sec:processeur_et_memoire}

Un CPU (pour \emph{Central Processing Unit}) ou processeur d'ordinateur
exécute une boucle conceptuellement simple~:
il consulte une adresse dans un registre appelé «~compteur ordinal~»,
lit une instruction machine depuis cette adresse en mémoire,
avance le compteur ordinal après l'instruction et exécute l'instruction.
Répéter.
Si l'exécution de l'instruction ne modifie pas le compteur ordinal, cette boucle
interprétera la mémoire pointée par le compteur ordinal comme
une séquence d'instructions à exécuter les unes à la suite des autres.
Les instructions qui modifient le compteur ordinal incluent les conditionnelles
et les apels de fonctions.

Le mécanisme d'exécution est inutile sans la capacité de stocker et de modifier
les données du programme.
Le stockage le plus rapide pour les données est fourni par l'ensemble
les registres processeurs.
Un registre est une cellule de stickage à l'intérieur du processeur lui-même,
capable de contenir une valeur de la taille d'un mot machine (généralement
16, 32 ou 64~bits).
Les données conservées dans ces registres peuvent être lues ou écrites
très rapidement, en un seul cycle processeur.

Les PC ont un processeur qui implémente le jeu d'instructions x86, qui a été
défini à l'origine par Intel et est devenu un standard.
Plusieurs fabriquants produisent des processeurs qui implémentent
ce jeu d'instruction. Comme tout les autres standards des PC il évolue,
mais les nouveaux standards sont toujours rétrocompatibles.
Le chargeur d'amorçage
\ndtshort{Le chargeur d'amorçage ou \emph{boot loader} est un logiciel
permettant le démarrage d'un ou de plusieurs systèmes d'exploitation.}
doit prendre en compte cette évolution puisque
chaque processeur de PC commence par simuler un Intel 8088,
la puce de processeur du IBM PC produit en 1981.
Toutefois, pour comprendre le fonctionnement de xv6, il vous faudra
être familier avec le jeu d'instructions x86 moderne.

Les x86 modernes fournissent huit registres 32~bits à usage général
---
\registre{eax},
\registre{ebx},
\registre{ecx},
\registre{edx},
\registre{edi},
\registre{esi},
\registre{ebp}
et
\registre{esp}
---
et un compteur ordinal
\registre{eip}
(le «~pointeur d'instructions~»).
Le préfixe commun \texttt{e} correspond à étendu, car il s'agit d'extensions
32~bits des registres 16~bits
\registre{ax},
\registre{bx},
\registre{cx},
\registre{dx},
\registre{di},
\registre{si},
\registre{bp},
\registre{sp}
et
\registre{ip}.
Les deux ensembles de registres sont en correspondance de sorte que par exemple,
\registre{ax} soit la moitié inférieure de \registre{eax}~:
écrire dans \registre{ax} modifie la valeur stockée dans \registre{eax}
et vice versa.
Les quatre premiers registres ont aussi un nom pour leur 8~bits
de poids faible~:
\registre{al} et \registre{ah} désignent les 8~bits de poids respectivement
forts et faible de \registre{ax}~;
\registre{bl}, \registre{bh}, \registre{cl}, \registre{ch},
\registre{dl} et \registre{dh} suivent ce modèle.
En plus de ces registres, x86 possède huit registres à virgule flottante
de 80~bits ainsi qu'une poignée de registres spéciaux comme
les «~registres de contrôle~» \registre{cr0}, \registre{cr2}
\ndtshort{Le registre \registre{cr1} est réservé au processeur.}
, \registre{cr3}
et \registre{cr4}~;
les registres de débogage \registre{dr0}, \registre{dr1}, \registre{dr2},
et \registre{dr3}~;
les «~registres de segments~» \registre{cs}, \registre{ds}, \registre{es},
\registre{fs}, \registre{gs} et \registre{ss}~;
et les pseudo-registres de la table des descripteurs locale et globale
\registre{ldtr} et \registre{gdtr}.
Les registres de contôle et les registres de segments sont importants pour
tout système d'exploitation.
Les registres à virgule flottante et les registres de débogage sont moins
intéressants et ne sont pas utilisés par xv6.

Les registres sont rapides mais coûtent cher.
La plupart des processeurs fournissent au plus quelques dizaine de registres
à usage général.
Le prochain niveau conceptuel de stockage est la mémoire vive principale
(ou RAM pour \emph{Random-Access Memory}).
La mémoire principale est 10 à 100 fois plus lente qu'un registre,
mais elle est beaucoup moins coûteuse, on peut donc en avoir plus.
Une des raisons qui rend la mémoire principale relativement lente est
qu'elle est physiquement séparé de la puce de processeur.
Un processeur x86 possède une petite douzaine de registres, mais un PC actuel
comprend généralement des gigabytes de mémoire principale.
En raison de cette énorme différence à la fois en temps d'accès et en taille
entre les registres et la mémoire principale, la plupart des processeurs,
dont le x86, conservent des copies de sections de la mémoire principale
récemment accédées dans une mémoire cache.
La mémoire cache sert d'intermédiaire entre les registres et la mémoire
à la fois en temps d'accès et en taille.
Aujourd'hui, les processeurs x86 possèdent générallement trois niveaux
de caches.
Chaque cœur a un petit cache de premier niveau avec des temps d'accès
relativement proches de la fréquence d'horloge du processeur
ainsi qu'un cache de second niveau plus grand.
Plusieurs cœurs se partagent un même cache de niveau 3.

\begin {figure}[t]
  \begin {center}
    \small
    \begin{tabular}{|c|r@{\hskip 3pt}l|r@{\hskip 3pt}l|}
      \hline
      \textbf{Mémoire} & \multicolumn{2}{c|}{\textbf{Temps d'accès}}
                       & \multicolumn{2}{c|}{\textbf{Taille}} \\ \hline
      registres & 1 & cycle & 64 & octets \\ \hline
      Cache L1 & \textasciitilde{}4 & cycles & 64 & kilooctets \\ \hline
      ache L2 & \textasciitilde{}10 & cycles & 4 & megaoctets \\ \hline
      Cache L3 & \textasciitilde{}40-75 & cycles & 8 & megaoctets \\ \hline
      Cache L3 distant & \textasciitilde{}100-300 & cycles & &  \\ \hline
      DRAM locale & \textasciitilde{}60 & nsec & & \\ \hline
      DRAM distante & \textasciitilde{}100 & nsec & & \\
      \hline
    \end{tabular}
  \end {center}
  \caption {Temps de latence pour un système Intel Xeon, basés sur
    \url{http://software.intel.com/sites/products/collateral/hpc/vtune/performance_analysis_guide.pdf}.}
  \label{fig:xeon}
\end {figure}

La majorité des processeurs x86 masquent les caches aux systèmes d'exploitation,
on peut donc se représenter un processeur comme ayant simplement deux sortes
de stockage --- les registres et la mémoire --- sans se soucier des distinctions
entre les différents niveaux de la hiérarchie de la mémoire.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Entrées/Sorties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Entrées/Sorties}
  \label{sec:entrees_sorties}

Les processeurs doivent communiquer aussi bien avec les périphériques
qu'avec la mémoire.
Le processeur de x86 fournit les instruction spéciales 
\codestyle{in} et \codestyle{out} qui lisent et écrivent des valeurs depuis
les adresses du périphérique appelées «~ports d'entrées/sorties~».
L'implémentation matérielle de ces instructions est essentiellement la même
que lire ou écrire dans la mémoire. Les premiers processeurs x86
possédaient un lien d'adresse supplémentaire~:
0 signifiait lire/écrire depuis un port d'entrées/sorties et 1
signifiait lire/écrire depuis la mémoire principale.
Chaque périphérique matériel surveille ces liens pour les lectures
et les écritures dans la plage de ports d'entrées/sorties qui lui est attribuée.
Les ports d'un périphérique permettent au logiciel de configurer
ce périphérique, d'examiner son statut et de lui demander d'effectuer
des actions~; par exemple, les logiciels peuvent utiliser les lectures
et écritures des ports d'entrées/sorties pour amener l'interface matériel
du disque à lire et écrire des secteurs sur le disque.

De nombreuses architectures d'ordinateurs ne possèdent pas d'instructions
d'accès aux périphériques spécifiques. À la place, les périphériques possèdent
des adresses mémoires fixes et le processeur communique avec un périphérique
(sous ordre du système d'exploitation) en lisant et écrivant des valeurs
à ces adresses. Dans les faits, les architectures x86 modernes utilisent
cette technique appelée «~\emph{adressage des entrées/sorties en mémoire}~»
pour la majorité des périphériques à haut débit tels que
les contrôleurs réseaux, disques et graphiques.
Pour des raisons de rétrocompatibilité, les anciennes instructions
\codestyle{in} et \codestyle{out} persistent, tout comme les périphériques qui
les utilisent, comme les controlleur de disque IDE, qu'utilise xv6.
