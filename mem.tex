\chapter {Tables de pages}
  \label{chap:mem}

Les tables de pages sont le mécanisme par lequel les systèmes
d'exploitation contrôlent la signification d'une adresse mémoire.
Ils permettent à xv6 de multiplexer les espaces d'adressage
de différents processus dans une même mémoire physique, et de protéger
les mémoires des différents processus.
Le niveau d'indirection fourni par les tables de pages permet
de nombreuses astuces.
Xv6 utilise principalement les tables de pages pour multiplexer
les espaces d'adressage et protéger la mémoire.
Il utilise aussi quelques astuces simples permises par les tables de pages~:
lier la même mémoire (le noyau) dans différents espaces d'adressage,
lier la même mémoire plus d'une fois dans un espace d'adressage
(chaque page utilisateur est ainsi liée à la mémoire physique du noyau)
et conserver une pile utilisateur sur une page non liée.
Le reste de ce chapitre explique les tables de pages fournies par
le x86 et comment xv6 les utilise.
Si on compare avec les systèmes d'exploitation réels,
la conception de xv6 est restreinte mais illustre les idées clefs.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% La MMU
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {La MMU}
  \label{sec:la_mmu}

Pour mémoire, les instructions x86 (à la fois en mode utilisateur ou noyau)
manipulent
des adresses virtuelles.
La RAM, ou mémoire physique, est indexée par des adresses physiques.
La MMU du x86 relie ces deux sortes d'adresses en traduisant chaque
adresse virtuelle en une adresse physique.

Une table des pages x86 est donc tout logiquement un tableau de $2^{20}$
(\numprint{1048576}) entrées, ou PTE (\emph{Page Table Entries\/}).
Chaque PTE contient un numéro de page physique sur 20~bits
ou PPN (\emph{Physical Page Number\/}) ainsi que quelques indicateurs.
La MMU traduit une adresse virtuelle en utilisant ses 20~bits de poids fort
comme index dans la table des pages afin de trouver une entrée et en remplaçant
les 20~bits de poids fort de l'adresse avec le PPN issu de la PTE.
La MMU recopie les 12~bits de poids faible, les gardant inchangés durant
la traduction de l'adresse virtuelle vers l'adresse physique.
Ainsi, la table des pages donne au système d'exploitation le contrôle sur
la traduction des adresses virtuelles vers des adresses physiques,
à la granularité de zones alignées et
de taille \numprint{4096} ($2^{12}$) octets.
Un telle zone est appelée \emph{page}.

\begin {figure}[t]
  \begin {center}
	  \includegraphics [width=10cm] {\figpath{x86_pagetable.pdf}}
  \end {center}
  \caption {MMU du x86.}
  \label{fig:x86_pagetable}
\end {figure}

Comme le montre la figure \ref{fig:x86_pagetable}, la traduction
à proprement parler se fait en deux étapes.
Une table des pages est stockée en mémoire physique comme un arbre
à deux niveaux.
La racine de cet arbre est le \emph{répertoire des pages} de
\numprint{4096}~octets,
qui contient \numprint{1024} références, similaires aux PTE, à des
\emph{pages de table des pages}.
Chaque page de table des pages est un tableau de \numprint{1024} PTE de 32~bits.
La MMU utilise les 10 bits de poids fort d'une adresse virtuelle pour
sélectionner une entrée du répertoire de pages.
Si cette entrée est présente, la MMU utilise les 10 bits
suivants de l'adresse virtuelle pour sélectionner une PTE depuis la page
de tables de pages référencée par l'entrée du répertoire de pages.
La MMU signale un défaut de page
si l'entrée du répertoire de pages ou la PTE n'est pas présente,
Cette structure à deux niveaux permet à une table des pages d'omettre
des pages de table des pages entières dans le cas fréquent pour lequel
de grandes plages d'adresses virtuelles n'ont pas de traduction.

Chaque PTE contient des bits (indicateurs) qui indiquent à la MMU la façon dont
l'adresse virtuelle associée est autorisée à être utilisée.
\codestyle{PTE\_P} indique si la PTE est présente~: s'il n'est pas activé,
une référence à cette page provoquera une erreur (c'est-à-dire qu'une telle
référence n'est pas autorisée).
\codestyle{PTE\_W} contrôle si les instructions sont autorisées à effectuer
des écritures sur la page~; s'il est non défini, seules les lectures de
données ou d’instructions sont autorisées.
\codestyle{PTE\_U} contrôle si les programmes en mode utilisateur sont autorisés
à utiliser cette page~; si cet indicateur est désactivé, seul le noyau
est autorisé à utiliser la page.
La figure \ref{fig:x86_pagetable} montre comment tout cela fonctionne.
Les indicateurs et toutes les autres structures en relation avec la MMU
sont définis dans \fichier{mmu.h} [\xvline{mmu.h:/.. This file/}].

Quelques précisions sur les termes.
La mémoire physique fait référence aux cellules de stockage de la DRAM.
Un octet de mémoire physique est situé à une adresse, appelée adresse physique.
Les instructions n'utilisent que des adresses virtuelles, que la MMU traduit
en adresses physiques puis envoie au composant matériel DRAM
pour lire ou écrire dans
les cellules de stockage.
À ce stade de l'explication, il n'existe que des adresses virtuelles,
la mémoire virtuelle, quant à elle, n'existe pas.

\begin {figure}[t]
  \begin {center}
	  \includegraphics [width=10cm] {\figpath{xv6_layout.pdf}}
  \end {center}
  \caption {
    Disposition de l'espace d'adressage virtuel et physique d'un processus.
    Notez que si une machine possède plus de 2~Go de mémoire physique,
    xv6 ne peut utiliser que l'espace compris entre \adresse{KERNBASE} et
    \adresse{0xFE00000}.
  }
  \label{fig:xv6_layout}
\end {figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Espace d'adressage d'un processus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Espace d'adressage d'un processus}
  \label{sec:espace_d_adressage_d_un_processus}

La table des pages créée par \xvf{entry} possède suffisamment d'entrées
pour commencer l'exécution du code C du noyau.
Cependant, \xvf{main} installe immédiatement une nouvelle table des pages
dans \xvfct{kvmalloc}{vm.c:/^kvmalloc/} car le noyau utilise une
description plus élaborée de l'espace d'adressage des processus.

Chaque processus a une table des pages séparée et xv6 indique à la MMU
de changer de table des pages lorsqu'il change de processus.
Comme le montre la figure \ref{fig:xv6_layout}, un espace mémoire utilisateur
commence à l'adresse virtuelle zéro et peut s'étendre jusqu'à
\adresse{KERNBASE}, permettant à un processus d'adresser jusqu'à 2~gigaoctets
de mémoire.
Le fichier \fichier{memlayout.h} [\xvline{memlayout.h:/.. Memory layout/}]
déclare les constantes pour l'agencement de la mémoire de xv6, et les macros
pour convertir des adresses virtuelles en adresses physiques.

Lorsqu'un processus demande à xv6 plus de mémoire, xv6 commence par trouver
une page physique libre pour fournir le stockage, puis ajoute les PTE
à la table des pages du processus qui pointe sur cette nouvelle page physique.
Xv6 active les indicateurs \codestyle{PTE\_U}, \codestyle{PTE\_W}
et \codestyle{PTE\_P} pour ces PTE.
La plupart des processus n'utilisent pas l'intégralité de l'espace d'adressage~;
xv6 laisse l'indicateur \codestyle{PTE\_P} désactivé dans les PTE inutilisées.
Les tables de pages de différents processus traduisent des adresses utilisateurs
vers des pages différentes de la mémoire physique, de sorte que chaque processus
possède sa propre mémoire utilisateur privée.

Xv6 inclut toutes les traductions nécessaires
pour que le noyau puisse s'exécuter
dans les tables de pages de chaque processus~; ces traductions apparaissent
toutes après \adresse{KERNBASE}.
Xv6 associe les adresses virtuelles \adresse{KERNBASE:KERNBASE+PHYSTOP}
aux adresses physiques \adresse{0:PHYSTOP}.
Une des raisons de cette traduction est de faire en sorte que le noyau puisse
utiliser ses propres instructions et données.
Une autre raison est que le noyau doit être parfois capable d'écrire
dans une page donnée de la mémoire physique, par exemple lors de la création
de pages de table des pages~; pour ce faire, il est pratique que
chaque page physique apparaisse à une adresse virtuelle prédictible.
Un inconvénient de cet agencement est que xv6 ne peut pas utiliser plus de 2~Go
de mémoire physique, car la partie noyau de l'espace d'adressage est de 2~Go.
Ainsi, xv6 requiert que \adresse{PHYSTOP} soit plus petit que 2~Go, même si
l'ordinateur possède plus de 2~Go de mémoire physique.

Certains périphériques qui utilisent l'adressage des entrées/sorties en mémoire
apparaissent à des adresses physiques commençant à \adresse{0xFE000000},
c'est pourquoi les tables de pages de xv6 incluent une traduction
directe pour eux.
Ainsi, \adresse{PHYSTOP} doit être plus petit que 2~Go - 32~Mo (pour
la mémoire des périphériques).

Xv6 n'active pas l'indicateur \codestyle{PTE\_U} dans les PTE au-delà
de \adresse{KERNBASE}, donc seul le noyau peut les utiliser.

Avoir une traduction d'adresses à la fois pour la mémoire utilisateur et
pour tout le noyau par la table des pages de tous les processus est commode
lors du basculement entre code utilisateur et code noyau pendant
les appels système et les interruptions~: de tels basculements ne nécessitent
pas de changement de table des pages.
Dans la plupart des cas, le noyau ne possède pas sa propre table des pages~;
il emprunte presque toujours la table des pages d'un processus.

Pour résumer, xv6 s'assure que chaque processus ne peut utiliser que
sa propre mémoire. Chaque processus voit cette mémoire comme ayant
des adresses virtuelles contiguës commençant à zéro, tandis que
la mémoire physique du processus peut ne pas être contiguë.
Xv6 implémente le premier paradigme en activant l'indicateur \codestyle{PTE\_U}
uniquement sur les PTE d'adresses virtuelles faisant référence à la mémoire
du processus lui-même.
Il implémente le second en utilisant la capacité des tables de pages à traduire
des adresses virtuelles successives en n'importe quelle page physique
allouée au processus.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : créer un espace d'adressage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: créer un espace d'adressage}
  \label{sec:code_creer_un_espace_d_adressage}

\xvf{Main} appelle \xvfct{kvmalloc}{vm.c:/^kvmalloc/} pour créer et basculer
vers une table des pages contenant des traductions pour les adresses supérieures
à \adresse{KERNBASE} indispensables pour l'exécution du noyau.
La plus grande partie de ce travail est faite par
\xvfct{setupkvm}{vm.c:/^setupkvm/}.
Cette fonction commence par allouer une page de mémoire pour contenir
le répertoire des pages.
Puis, elle appelle \xvf{mappages} pour installer les traductions dont le noyau
a besoin, décrites dans le tableau \codestyle{kmap} [\xvline{vm.c:/^..kmap/}].
La traduction inclut les données et instructions du noyau, la mémoire physique
jusqu'à \adresse{PHYSTOP} et des plages mémoires qui sont en réalité
des périphériques d'entrées/sorties.
\xvf{Setupkvm} n'installe aucune traduction pour la mémoire utilisateur~;
cela viendra plus tard.

\xvfct{Mappages}{vm.c:/^mappages/} installe des traductions dans
une table des pages pour une plage d'adresses virtuelles vers la plage
d'adresses physiques associée.
Il fait ceci séparément pour chaque adresse virtuelle dans la plage,
à des intervalles d'une page.
Pour chaque adresse virtuelle, \xvf{mappages} appelle \xvf{walkpgdir}
pour déterminer l'adresse de la PTE correspondante.
\xvf{Mappages} initialise ensuite cette PTE pour qu'elle contienne le bon numéro
de page physique, les permissions désirées (\codestyle{PTE\_W} et/ou
\codestyle{PTE\_U}) ainsi que \codestyle{PTE\_P} pour marquer la PTE
comme valide [\xvline{vm.c:/perm...PTE_P/}].

La fonction \xvfct{walkpgdir}{vm.c:/^walkpgdir/}
imite les actions de la MMU puisqu'elle
recherche dans les PTE une adresse virtuelle
(voir figure~\ref{fig:x86_pagetable}).
\xvf{Walkpgdir} utilise les 10 bits de poids fort de l'adresse virtuelle
pour trouver l'entrée du répertoire de pages [\xvline{vm.c:/pde.=..pgdir/}].
Si cette entrée n'est pas présente, alors la table de pages
demandée n'a pas encore été allouée~; si l'argument \codestyle{alloc}
vaut «~vrai~»,
\xvf{walkpgdir} alloue cette table et met son adresse physique dans
le répertoire des pages.
Enfin, \xvf{walkpgdir} utilise les 10 bits suivants
de l'adresse virtuelle pour trouver
l'adresse de la PTE dans la table des pages [\xvline{vm.c:/return..pgtab/}].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Allocation mémoire physique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Allocation de mémoire physique}
  \label{sec:allocation_memoire_physique}

Le noyau doit allouer et libérer la mémoire physique à l'exécution, que ce soit
pour les tables de pages, la pile noyau et les tampons
\ndtshort{Le terme original \emph{buffer} correspond à un espace mémoire
temporaire. Nous utiliserons dans la suite le terme \frquote{tampon},
bien que le terme original soit communément admis.}
de tubes.

Xv6 utilise la mémoire physique située entre la fin du noyau et
\adresse{PHYSTOP} pour les allocations au cours de l'exécution.
Il alloue et libère des pages entières de \numprint{4096}~octets à la fois.
Il conserve une trace des pages libres en faisant passer une liste chaînée
au travers des pages elles-mêmes.
L'allocation consiste en la suppression d'une page de la liste chaînée~;
la libération consiste en l'ajout de la page libre à la liste.

Il existe cependant un problème d'initialisation~:
toute la mémoire physique doit être
traduite pour permettre à l'allocateur d'initialiser la liste des pages libres,
mais créer une table des pages avec ces traductions implique d'allouer
une page de tables de pages.
Xv6 résout ce problème en utilisant durant l'initialisation
un allocateur de page séparé
qui alloue la mémoire juste après la fin du segment des données du noyau.
Cet allocateur ne supporte pas la libération et est limité par une traduction
de 4~Mo dans \xvf{entrypgdir}, mais cela reste suffisant pour allouer
la première table des pages du noyau.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : Allocateur de mémoire physique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: Allocateur de mémoire physique}
  \label{sec:code_allocateur_de_memoire_physique}

La structure de données de l'allocateur est une liste des pages physiques
de la mémoire disponibles pour l'allocation.
Chaque élément de cette liste est une \codestyle{struct run}
[\xvline{kalloc.c:/^struct.run/}].
Où l'allocateur trouve-t-il la place pour stocker cette structure de données~?
Il place chaque structure \codestyle{run} des pages disponibles au sein
de la page disponible elle-même, puisque rien d'autre n'y est stocké.
La liste des pages libres est protégée par un \emph{spin lock}
\ndtshort{Le terme \emph{spin lock} correspond à un \frquote{verrouillage avec
attente active}, mais nous avons préféré conserver l'expression anglaise,
plus courte.}
[\xvline{kalloc.c:/^struct.\{/,/\}/}].
La liste et le verrou sont emballés dans une structure pour clarifier
le fait que le verrou protège les champs de la structure.
Pour le moment, mieux vaut ignorer le verrou ainsi que les appels à
\xvf{acquire} et \xvf{release}~; le chapitre \ref{chap:lock} les détaillera.

La fonction \xvf{main} appelle \xvf{kinit1} et \xvf{kinit2} pour initialiser
l'allocateur [\xvline{kalloc.c:/^kinit1/}].
La raison qui justifie deux fonctions est que durant la plus grande
partie de \xvf{main},
il n'est pas possible d'utiliser de verrous ou de la mémoire au delà de 4~Mo.
L'appel à \xvf{kinit1} met en place une allocation sans verrou dans
les 4 premiers mégaoctets, tandis que l'appel à \xvf{kinit2} permet les verrous
et l'allocation de davantage de mémoire.
\xvf{Main} devrait déterminer la quantité de mémoire physique disponible,
mais cela s'avère difficile sur une architecture x86.
À la place, xv6 suppose que la machine possède 224~Mo (\codestyle{PHYSTOP})
de mémoire physique, et utilise toute la mémoire située entre la fin du noyau
et \codestyle{PHYSTOP} comme réservoir initial de mémoire libre.
\xvf{Kinit1} et \xvf{kinit2} appellent \xvf{freerange} pour ajouter
de la mémoire à la liste des pages vides en appelant pour chaque page
\xvf{kfree}.
Une PTE ne peut faire référence qu'à une adresse physique alignée sur une limite
de \numprint{4096}~octets (qui est un multiple de \numprint{4096}),
aussi \xvf{freerange} utilise
\codestyle{PGROUNDUP} pour s'assurer qu'il libère seulement des adresses
physiques alignées.
L'allocateur commence sans mémoire~; ces appels à \xvf{kfree} lui en donnent
à gérer.

L'allocateur fait référence à des pages physiques par leur adresse virtuelle
et non par leur adresse physique ;
c'est pourquoi \codestyle{kinit} utilise \codestyle{P2V(PHYSTOP)} pour traduire
\codestyle{PHYSTOP} (une adresse physique) en une adresse virtuelle.
L'allocateur traite parfois les adresses comme des entiers afin d'effectuer
des calculs arithmétiques (par exemple pour traverser toutes les pages
dans \codestyle{kinit}) et il les utilise parfois
comme des pointeurs
pour lire et écrire dans la mémoire (par exemple lorsqu'il manipule la structure
\codestyle{run} stockée dans chaque page)~; cette double utilisation est
la raison principale pour laquelle le code de l'allocateur est truffé
de conversions de types C (\emph{cast}).
L'autre raison est que la libération et l'allocation modifient
implicitement le type
de la mémoire.

La fonction \xvfct{kfree}{kalloc.c:/^kfree/} commence par mettre
tous les octets de la mémoire libérée à 1.  Ainsi, un code qui
utiliserait cette mémoire après libération (qui utiliserait donc des
\frquote{\emph{dangling references}}\ndtshort{Le terme \emph{dangling
reference} désigne une référence vers une partie de la mémoire qui
a été désallouée depuis la mise en place de cette référence. Une
traduction pourrait être \frquote{référence pendouillante},
mais nous avons préféré conserver l'expression originale.}) lira
des données erronées plutôt que d'anciennes données valides~;
avec un peu de chance, le code se plantera plus rapidement.  Ensuite,
\xvf{kfree} convertit \codestyle{v} en un pointeur sur \codestyle{struct
run}, sauvegarde l'ancien début de la liste des pages libres dans
\codestyle{r->next} et définit \codestyle{r} comme nouveau début de
la liste.  \xvf{Kalloc} supprime puis renvoie le premier élément de
la liste des pages libres.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Partie utilisateur d'un espace d'adressage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Partie utilisateur d'un espace d'adressage}
  \label{sec:partie_utilisateur_d_un_espace_d_adressage}


\begin {figure}[t]
  \begin {center}
	  \includegraphics [height=7cm] {\figpath{processlayout.pdf}}
  \end {center}
  \caption {
    Agencement en mémoire d'un processus utilisateur avec sa pile initiale.
  }
  \label{fig:processlayout}
\end {figure}

La figure \ref{fig:processlayout} montre l'agencement de
la mémoire utilisateur d'un processus en cours d'exécution avec xv6.
Chaque processus utilisateur commence à l'adresse 0.
La partie inférieure de l'espace d'adressage contient le
texte\ndtshort{Le terme \emph{text} correspond habituellement au
code binaire exécutable.}
du programme utilisateur, ses données et sa pile.
Le tas est situé au-dessus de la pile afin qu'il puisse s'étendre lorsque
le processus fait un appel à \xvf{sbrk}.
Il est à noter que les sections texte, données et pile sont disposées
de manière contiguë dans l'espace d'adressage du processus, mais xv6 est libre
d'utiliser des pages physiques non contiguës pour ces sections.
Par exemple, lorsque xv6 étend le tas d'un processus, il peut utiliser n'importe
quelle page physique libre pour la nouvelle page virtuelle, puis indiquer
à la MMU la traduction de la page virtuelle vers la page physique allouée.
Cette flexibilité est un avantage majeur de l'utilisation de la MMU.

La pile n'est constituée que d'une simple page, et figure sur le schéma avec
son contenu initial tel que créé par \xvf{exec}.
Tout en haut de la pile, on retrouve les chaînes de caractères contenant
les arguments entrés en ligne de commande, ainsi qu'un pointeur sur
ces derniers.
Juste en dessous se trouvent des valeurs permettant au programme de commencer
à partir de \xvf{main} comme si la fonction \codestyle{main(argc, argv)} venait
d'être appelée.
Pour se prémunir d'un débordement de la pile en dehors de sa page, xv6 place
une page de garde juste en dessous. La page de garde n'est
pas traduite, donc si la pile déborde de sa page, la MMU va générer
une exception car elle ne peut pas traduire l'adresse fautive.
Un vrai système d'exploitation pourrait allouer plus d'espace pour la pile
afin qu'elle puisse croître au-delà d'une page.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : sbrk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: sbrk}
  \label{sec:code_sbrk}

\xvf{Sbrk} est l'appel système qui permet à un processus de réduire ou
d'accroître son espace mémoire.
Cet appel système est implémenté par la fonction
\xvfct{growproc}{proc.c:/^growproc/}.
Si \codestyle{n} est positif, \xvf{growproc} alloue une ou plusieurs pages
et les associe au sommet de l'espace d'adressage du processus.
Si \codestyle{n} est négatif, \xvf{growproc} retire la traduction
d'une ou plusieurs pages
de l'espace d'adressage du processus et libère les pages physiques
correspondantes.
Pour effectuer ces changements, xv6 modifie la table des pages du processus.
Celle-ci est stockée en mémoire, ainsi le noyau
peut la mettre à jour en utilisant des affectations ordinaires, ce que font
\xvf{allocuvm} et \xvf{deallocuvm}.
La MMU du x86 met les entrées de la table des pages dans un
cache matériel
nommé \frquote{TLB} (\emph{Translation Look-aside Buffer\/}), et lorsque xv6
change de table des pages, il doit invalider les entrées de ce cache.
S'il ne le fait pas, le TLB pourrait ultérieurement utiliser
une ancienne traduction pointant sur une page physique qui aurait
été entretemps allouée à un autre processus et, par conséquent,
un processus pourrait être capable d'écrire dans la mémoire
d'un autre processus.
Xv6 invalide les entrées de cache obsolètes en rechargeant \registre{cr3},
le registre contenant l'adresse de la page des tables courante.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : exec
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code~: exec}
  \label{sec:code_exec}

\xvf{Exec} est l'appel système qui crée la partie utilisateur
d'un espace d'adressage.
Il initialise cette partie depuis un fichier stocké dans le système de fichiers.
\xvfct{Exec}{exec.c:/^exec/} ouvre le fichier binaire nommé \codestyle{path}
en utilisant \xvfct{namei}{exec.c:/namei/}, qui sera expliquée dans le chapitre
\ref{chap:fs}.
Puis, il lit l'en-tête ELF.
Les applications de xv6 sont dans le \frquote{format ELF}, format
largement répandu et décrit dans \fichier{elf.h}.
Un binaire ELF consiste en un en-tête ELF, \codestyle{struct elfhdr}
[\xvline{elf.h:/^struct.elfhdr/}], suivi d'une suite d'en-têtes de sections
du programme, \codestyle{struct proghdr} [\xvline{elf.h:/^struct.proghdr/}].
Chaque \codestyle{proghdr} décrit une section de l'application qui doit
être chargée en mémoire~; avec xv6, les programmes ne possèdent qu'un seul
en-tête de section, mais d'autres systèmes pourraient avoir des sections
séparées pour les instructions et les données.

La première étape est une rapide vérification que le fichier soit
bien un binaire au format ELF.
Un tel fichier commence par le nombre de quatre octets
\codestyle{0x7F}, \codestyle{'E'},
\codestyle{'L'}, \codestyle{'F'}, ou \codestyle{ELF\_MAGIC}
[\xvline{elf.h:/ELF\_MAGIC/}] appelé \frquote{nombre magique}.
Si l'en-tête ELF contient le bon nombre magique, \xvf{exec} considère que
le binaire est bien formé.

\xvf{Exec} alloue une nouvelle table des pages sans traduction
des adresses utilisateurs avec \xvf{setupkvm} [\xvline{exec.c:/allocuvm/}],
et charge chaque segment en mémoire avec \xvf{loaduvm}
[\xvline{exec.c:/loaduvm/}].
\xvf{Allocuvm} vérifie que l'adresse virtuelle demandée est inférieure à
\adresse{KERNBASE}.
\xvfct{Loaduvm}{vm.c:/^loaduvm/} utilise \xvf{walkpgdir} pour trouver l'adresse
physique de la mémoire fraichement allouée dans laquelle
placer chaque page du segment ELF,
et \xvf{readi} pour lire depuis le fichier.

L'en-tête de la section du programme \codestyle{/init},
le premier programme créé avec \xvf{exec} ressemble à cela~:
\code{init_section_header}

Le champ \codestyle{filesz} de l'en-tête de la section du programme peut être
plus petit que \codestyle{memsz}, indiquant que l'espace entre doit être comblé
par des zéros (pour les variables globales en C) plutôt que lues depuis
le fichier.
Pour \codestyle{/init}, \codestyle{filesz} vaut \numprint{2240}~octets
et \codestyle{memsz} vaut \numprint{2252}~octets,
ainsi \xvf{allocuvm} alloue suffisamment
d'espace mémoire physique pour contenir \numprint{2252}~octets,
mais ne lit que \numprint{2240}~octets
depuis le fichier \codestyle{/init}.

Maintenant, \xvf{exec} alloue et initialise la pile utilisateur.
Il alloue une page unique pour la pile.
\xvf{Exec} copie une par une les chaînes de caractères données
en arguments au sommet
de la pile, sauvegardant les pointeurs sur ces dernières dans
\codestyle{ustack}.
Il place un pointeur nul à la fin de ce qui sera la liste \codestyle{argv}
passée à \xvf{main}.
Les trois premières entrées dans \codestyle{ustack} sont la fausse
adresse de retour, ainsi que \codestyle{argc}
et le pointeur \codestyle{argv}.

\xvf{Exec} place une page inaccessible juste en dessous de la page de pile,
de sorte que les programmes qui essayent d'utiliser plus d'une page provoquent
une erreur.
Cette page inaccessible permet aussi à \xvf{exec} de gérer des arguments
trop grands~; dans une telle situation, la fonction
\xvfct{copyout}{vm.c:/^copyout/} qu'utilise \xvf{exec} pour copier les arguments
sur la pile va remarquer que la page de destination n'est pas accessible et
va renvoyer -1.

Pendant la préparation de la nouvelle image mémoire, si \xvf{exec} détecte
une erreur telle qu'une section de programme invalide, il se rend à l'étiquette
\codestyle{bad}, libère la nouvelle image et renvoie -1.
\xvf{Exec} doit attendre qu'il soit certain que l'appel système
réussisse avant de libérer l'ancienne image :
si l'ancienne image était perdue, l'appel système ne pourrait pas
renvoyer -1.
Les seuls cas d'erreur dans \xvf{exec} se produisent lors de la création
d'une image.
Une fois l'image complète, \xvf{exec} peut installer cette nouvelle image
[\xvline{exec.c:/switchuvm/}] et libérer l'ancienne [\xvline{exec.c:/freevm/}].
Enfin, \xvf{exec} renvoie 0.

\xvf{Exec} charge des octets depuis un fichier ELF vers la mémoire à
des adresses spécifiées dans le fichier ELF.
Les utilisateurs et les processus peuvent placer les adresses de leur choix
dans un fichier ELF.
\xvf{Exec} est donc risqué car les adresses dans le fichier ELF peuvent
faire référence, accidentellement ou non, au noyau.
Les conséquences pour un noyau imprudent peuvent aller du \emph{crash}
à la corruption des mécanismes d'isolation du noyau (c'est-à-dire
une faille de sécurité).
Xv6 effectue un certain nombre de vérifications pour éviter ces risques.
Pour comprendre l'importance de ces vérifications, considérons
ce qui se passerait si xv6 ne vérifiait pas
\codestyle{if(ph.vaddr + ph.memsz < ph.vaddr)}.
Il s'agit d'une vérification pour savoir si la somme dépasse un entier
de 32~bits.
Le danger est qu'un utilisateur pourrait construire un binaire ELF avec 
un \codestyle{ph.vaddr} qui pointerait dans le noyau, et \codestyle{memsz}
suffisamment grand pour que la somme dépasse 0x1000.
Tant que la somme est petite, elle passerait la vérification
\codestyle{if(newsz >= KERNBASE)} dans \xvf{allocuvm}.
L'appel ultérieur à \xvf{loaduvm} transmettrait \codestyle{ph.vaddr}
directement, sans ajouter \codestyle{ph.memsz} et sans vérifier
\codestyle{ph.vaddr} avec \codestyle{KERNBASE}, et copierait ainsi des données
depuis le binaire ELF dans le noyau.
Ceci pourrait être exploité par un programme utilisateur pour exécuter
un code utilisateur arbitraire avec les privilèges du noyau.
Comme l'illustre cet exemple, la vérification des arguments doit être faite
avec beaucoup de soin.
Il est facile pour un développeur du noyau d'omettre une vérification cruciale,
et les noyaux réels possèdent une longue histoire de vérifications manquantes,
dont l'absence peut être exploitée par des programmes utilisateurs pour obtenir
les privilèges du noyau.
Il est probable que xv6 n’effectue pas un travail complet de validation
des données utilisateurs fournies au noyau, ce qu'un programme utilisateur
malveillant pourrait exploiter pour contourner l'isolation offerte par xv6.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dans la réalité
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Le monde réel}
  \label{sec:dans_la_realite_mem}

Comme la plupart des systèmes d'exploitation, xv6 utilise la MMU
pour la protection de la mémoire et la traduction des adresses.
La plupart des systèmes d'exploitation utilise la MMU x86 64~bits (qui possède
3 niveaux de traduction).
Des espaces d'adressages de 64~bits permettent une disposition mémoire moins
restrictive que celle de xv6~; il serait par exemple plus simple de supprimer
la limite de 2~Gigaoctets de mémoire physique de xv6.
La plupart des systèmes d'exploitation font une utilisation bien plus
sophistiquée de la pagination que xv6~; par exemple, xv6 ne fait pas
de pagination depuis le disque, de fork avec copie à l'écriture,
de mémoire partagée, d'allocation paresseuse des pages ni d'extension
automatique de la pile.
Le x86 supporte la traduction d'adresses en utilisant la segmentation (voir
annexe \ref{app:boot}), mais xv6 n'utilise la segmentation que pour l'astuce
courante d'implémentation de variables propres aux processeurs comme
\codestyle{proc}, qui sont à des adresses fixées mais ont des valeurs
différentes pour chaque processeur (voir \codestyle{seginit}).
Les implémentations d'un stockage par processeur (ou par thread) sur
des architectures non segmentées dédieraient un registre pour stocker
un pointeur sur une zone donnée propre au processeur, mais le x86 possède
si peu de registres généraux que le supplément d'effort requis par
la segmentation en vaut la peine.

Xv6 associe le noyau à l'espace d'adressage
de chaque processus utilisateur, mais
il l'installe de sorte que la partie noyau de l'espace d'adressage soit
inaccessible lorsque le processeur est en mode utilisateur.
Cette organisation est pratique car après qu'un processus a basculé
de l'espace utilisateur vers l'espace noyau, le noyau peut facilement accéder
à la mémoire utilisateur en lisant directement les emplacements en mémoire.
Il est toutefois probablement mieux, pour la sécurité, d'avoir une table
des pages pour le noyau et d'utiliser cette table des pages lorsqu'on entre
dans le noyau depuis le mode utilisateur, de sorte que les processus noyaux
et utilisateurs soient davantage séparés les uns des autres.
Une telle conception par exemple, aiderait à atténuer les canaux auxiliaires,
exposés par la vulnérabilité Meltdown qui permet à un processus utilisateur
de lire n'importe quelle partie de la mémoire du noyau.

Sur des machines avec beaucoup de mémoire, il pourrait être logique d'utiliser
les \frquote{super-pages} de 4~mégaoctets du x86.
L'utilisation de petites pages est logique lorsque la mémoire physique
est restreinte pour permettre une granularité fine de l'allocation et
de l'évincement des pages sur le disque.
Par exemple, si un programme n'utilise que 8~Ko de mémoire, lui donner des pages
physiques de 4~Mo serait du gaspillage.
L'utilisation de pages plus grandes a du sens sur des machines avec beaucoup
de RAM, et peut réduire les coûts liés à la manipulation des tables de pages.
Xv6 n'utilise les super-pages qu'à un endroit~: la table des pages initiale
[\xvline{main.c:/^pde_t.entrypgdir.*=/}].
L'initialisation du tableau modifie deux des 1024 PDE, aux indices 0 et 512
(\codestyle{KERNBASE>>PDXSHIFT}), laissant les autres à zéro.
Xv6 active l'indicateur \codestyle{PDE\_PS} dans ces deux PDE pour les marquer
comme étant des super-pages.
Le noyau indique également à la MMU d'autoriser les super-pages en activant
le bit \codestyle{CR\_PSE} (\emph{Page Size Extension}) dans le registre
\registre{cr4}.

Xv6 devrait déterminer la configuration actuelle de la RAM, au lieu de
la supposer à 224 Mo.
Sur le x86, il existe au minimum trois algorithmes classiques~:
le premier est de sonder l'espace d'adressage physique en cherchant des zones
qui se comportent comme de la mémoire, conservant les valeurs qu'on y écrit~;
le deuxième consiste à lire le nombre de kilo-octets de mémoire en dehors
d'un emplacement connu de 16~bits dans la RAM non volatile du PC~;
et le troisième est de chercher dans la mémoire du BIOS la table d'agencement
de la mémoire faisant partie des tables multiprocesseurs.
Lire le tableau de disposition de la mémoire est compliqué.

L'allocation mémoire était un sujet brûlant il y a longtemps,
le problème de base étant un usage efficient d'une quantité de mémoire limitée
et la préparation de futures requêtes inconnues~; cf. Knuth.
Aujourd'hui, on se soucie plus de la vitesse que de l'optimisation mémoire.
De plus, un noyau plus élaboré aurait probablement alloué plusieurs tailles
différentes de petits blocs, plutôt que simplement des blocs de
\numprint{4096}~octets
(comme dans xv6)~; un vrai allocateur noyau devrait avoir à gérer
de petites allocations comme des grandes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exercices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Exercices}
  \label{sec:exercices_mem}

\begin{enumerate}
  \item{
    Renseignez-vous sur le fonctionnement de réels systèmes d'exploitation pour
    voir comment ils dimensionnent la mémoire.
  }
  \item{
    Si xv6 n'avait pas utilisé les super-pages, quelle aurait été la bonne
    déclaration pour \codestyle{entrypgdir} ?
  }
  \item{
    Écrivez un programme utilisateur qui agrandit son espace d'adressage de
    1~octet en appelant \codestyle{sbrk(1)}.
    Exécutez le programme et examinez la table des pages du programme avant
    et après l'appel à \codestyle{sbrk}.
    Combien d'espace le noyau a-t-il alloué ?
    Que contient le \codestyle{pte} de la nouvelle mémoire ?
  }
  \item{
    Modifiez xv6 afin que les pages pour le noyau soient partagées entre
    les processus, ce qui réduit la consommation mémoire.
  }
  \item{
    Modifiez xv6 afin que lorsqu'un programme utilisateur déréférence
    un pointeur nul, il reçoive une erreur~; c'est-à-dire, modifiez xv6 afin que
    l'adresse virtuelle 0 ne soit plus traduite pour
    les programmes utilisateurs.
  }
  \item{
   Les implémentations Unix de \codestyle{exec} incluent traditionnellement
   un traitement spécial pour les scripts shell.
   Si le fichier à exécuter commence par \frquote{\codestyle{\#!}},
   alors la première ligne est considérée comme étant un programme à exécuter
   pour interpréter le fichier.
   Si par exemple, \codestyle{exec} était appelé pour exécuter
   \codestyle{myprog arg1} et que la première ligne de \codestyle{myprog}
   était \codestyle{\#!/interp}, alors \codestyle{exec} devrait exécuter
   \codestyle{interp} avec la ligne de commande \codestyle{/interp myprog arg1}.
   Implémentez la prise en charge de cette convention dans xv6.
  }
  \item{
    Supprimez la vérification \codestyle{if(ph.vaddr + ph.memsz < ph.vaddr)}
    dans \fichier{exec.c} et construisez un programme utilisateur
    qui exploite l'absence de cette vérification.
  }
  \item{
    Modifiez xv6 afin que les processus utilisateurs s'exécutent avec une partie
    minimale de la traduction des adresses du noyau,
    et de sorte à ce que le noyau
    s'exécute avec sa propre table des pages qui n'inclut pas le processus
    utilisateur. 
  }
  \item{
    Comment amélioreriez-vous la disposition mémoire de xv6 si xv6 s'exécutait
    sur un processeur 64~bits ? 
  }
\end{enumerate}
