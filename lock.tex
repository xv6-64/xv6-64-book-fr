\chapter {Verrouillage}
  \label{chap:lock}

Xv6 est adapté à des multiprocesseurs~:
des ordinateurs avec plusieurs processeurs tournant indépendamment.
Ces différents processeurs partagent la RAM physique et xv6 exploite ce partage
pour maintenir des structures de données que tous les processeurs
lisent et écrivent.
Ce partage engendre le risque qu'un processeur lise une structure
de données pendant qu'un autre processeur n'a pas complètement terminé de
la modifier, ou même que plusieurs processeurs modifient la même structure
de données simultanément~; sans une conception rigoureuse, de tels
accès parallèles sont susceptibles de conduire à des résultats incorrects
ou une structure de données incohérente.
Même sur une machine avec un seul processeur, une routine d'interruption
qui utilise les mêmes données que du code interruptible pourrait
altérer les données si l'interruption survenait au moment crucial.

Tout code qui accède simultanément à des données partagées doit avoir
une stratégie pour préserver l'exactitude malgré la concurrence.
Celle-ci peut provenir de l'accès par de multiples cœurs de processeurs,
par de multiples threads, ou par du code d'interruption.
Xv6 met en œuvre quelques stratégies simples pour contrôler la
concurrence~; une plus grande sophistication est possible.
Ce chapitre se concentre sur l'une des stratégies abondamment utilisée
dans xv6 et dans beaucoup d'autres systèmes : le \emph{verrouillage}.

Un verrou fournit l'exclusion mutuelle, en faisant en sorte qu'un
seul processeur à un instant donné puisse
obtenir\ndtshort{Normalement, un verrou physique sur une porte s'ouvre
ou se ferme. Dans le contexte de la concurrence, on \emph{possède}
ou on \emph{libère} le verrou.} le verrou. Si un verrou est associé
avec chaque donnée partagée, et que le code obtient toujours le
verrou associé quand il utilise cette donnée, alors on peut être
sûr que la donnée est utilisée par un seul processeur à la fois.
Dans cette situation, on dit que le verrou protège la donnée.

Le reste de ce chapitre explique pourquoi xv6 a besoin de verrous,
comment il les implémente et comment il les utilise.
Le point clé, lorsque vous lisez du code de xv6, est de se
demander si un autre processeur (ou une interruption) pourrait
modifier le comportement attendu du code en modifiant les données
(ou les ressources matérielles) dont il dépend.
Vous devez garder en tête qu'une seule instruction C peut correspondre
à plusieurs instructions machines,
et que donc un autre processeur ou une interruption pourrait
survenir au milieu d'une instruction C.
Vous ne pouvez pas supposer que des lignes de code sur le papier sont
exécutées de manière atomique.
La concurrence rend les raisonnements sur l'exactitude beaucoup plus
difficiles.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conditions de concurrence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Conditions de concurrence}

Prenons un exemple pour expliquer pourquoi nous avons besoin de verrous :
considérons plusieurs processeurs partageant un disque unique,
tel qu'un disque IDE dans xv6. Le pilote du disque maintient une
liste chaînée des requêtes disques en attente (\xvline{ide.c:/idequeue/})
et les processeurs peuvent en ajouter de nouvelles en même temps
(\xvline{ide.c:/^iderw/}).
S'il n'y avait pas de requêtes concurrentes, il serait possible
d'implémenter la liste chaînée comme suit :
\code{ex_llist.c}

\begin {figure} %[t]
  \begin {center}
	  \includegraphics [width=.6\linewidth] {\figpath{race.pdf}}
  \end {center}
  \caption {Exemple de condition de concurrence.}
  \label{fig:race}
\end {figure}

Cette implémentation est correcte si elle est exécutée en isolation.
Cependant, le code n'est pas correct si plus d'une copie l'exécute
en même temps. Si deux processeurs exécutent \codestyle{insert}
au même moment, il pourrait arriver qu'ils exécutent tous les
deux la ligne 15 avant que l'un des deux exécute la ligne 16 (voir
figure~\ref{fig:race}). Si cela arrive, il y aura alors
deux éléments de la liste pour lesquels \codestyle{next} vaut la
précédente valeur de \codestyle{list}. Quand les deux affectations
à \codestyle{list} à la ligne 16 seront effectuées, la deuxième
aura écrasé la première~; l'élément de la liste utilisé dans la
première affectation sera perdu.

La mise à jour perdue en ligne 16 est un exemple de \emph{condition de
concurrence}\ndtshort{Le terme original est \emph{race condition}.}. Une
condition de concurrence est une situation dans laquelle un emplacement
mémoire est accédé concurremment, et au moins un des accès est une
écriture. Une telle condition est souvent le signe d'un bogue, soit
d'une mise à jour perdue (si les accès sont des écritures), soit d'une
lecture d'une structure de données alors que la mise à jour de celle-ci
n'est pas terminée. Le résultat de cette condition dépend du timing
précis des deux processeurs impliqués et de comment les opérations
en mémoire sont ordonnées par le système mémoire, ce qui peut rendre
les erreurs dues à des conditions de concurrence difficiles à reproduire.
Par exemple, le fait d'ajouter des instructions d'affichage
pendant le déboguage de \codestyle{insert} peut suffisamment changer
le timing de l'exécution pour faire disparaître le problème.

La méthode classique pour éviter les conditions de concurrence
est d'utiliser un verrou. Les verrous garantissent l'\emph{exclusion
mutuelle}, de telle façon qu'un seul processeur à la fois puisse
exécuter \codestyle{insert}~; ceci rend le scénario décrit ci-dessus
impossible. La version correctement verrouillée du code précédent
ajoute juste quelques lignes (non numérotées) :

\code{ex_llist_lock.c}

La suite d'instructions entre \codestyle{acquire} et \codestyle{release}
est souvent appelée une \emph{section critique}, et le verrou protège
\codestyle{list}.

Quand on dit qu'un verrou protège une donnée, nous voulons vraiment
dire que le verrou protège un ensemble d'invariants qui s'appliquent
à la donnée. Les invariants sont des propriétés d'une structure de
données qui sont maintenues à travers des opérations. Typiquement,
le comportement correct d'une opération dépend du fait que les
invariants sont vérifiés lorsque l'opération débute.
L'opération peut temporairement violer les invariants, mais doit les
rétablir avant de terminer.
Par exemple, dans le cas de la liste chaînée, l'invariant est
\frquote{\codestyle{list} pointe sur le premier élément de la liste et
le \codestyle{next} de chaque élément pointe sur l'élément suivant}.
L'implémentation de \codestyle{insert} viole temporairement cet
invariant~: à la ligne 15, \codestyle{l} pointe sur l'élément suivant,
mais \codestyle{list} ne pointe pas encore sur \codestyle{l} (ce qui
est rétabli à la ligne 16).
La condition de concurrence que nous avons examinée ci-dessus arrive
parce qu'un second processeur a exécuté du code qui dépendait de
l'invariant de liste, alors que nous l'avions (temporairement) violé.
Une utilisation correcte d'un verrou garantit qu'un seul processeur
à la fois peut opérer sur la structure de données dans la section
critique, de telle façon qu'aucun autre processeur ne puisse exécuter
d'opération sur cette structure de données tant que les invariants ne
sont pas vérifiés.

On peut assimiler l'utilisation des verrous à une \emph{sérialisation}
des sections critiques concurrentes, de telle sorte qu'elles soient
exécutées une seule à la fois, et donc en préservant les invariants
(en supposant qu'ils soient corrects en isolation). On peut aussi voir
les sections critiques comme étant atomiques les unes par rapport
aux autres, de telle façon qu'une section critique qui obtient le
verrou ultérieurement verra uniquement l'ensemble complet des
modifications effectuées par les sections critiques antérieures,
et ne verra jamais les mises à jour incomplètes.

Notez qu'il serait également correct de remonter \codestyle{acquire}
plus haut dans \codestyle{insert}. Par exemple, il est acceptable de
remonter l'appel à \codestyle{acquire} jusqu'à avant la ligne 12.
Cela peut réduire le parallélisme car les appels à \codestyle{malloc}
sont également sérialisés.
La section \ref{sec:utilisation-des-verrous}
(page~\pageref{sec:utilisation-des-verrous}) donne des indications
pour savoir où insérer des appels à \codestyle{acquire} et
\codestyle{release}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code : verrous
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code : Verrous}

Xv6 dispose de deux types de verrous : les verrous actifs\ndtshort{Le terme
original est \emph{spin-lock}.} et les verrous passifs\ndtshort{Le terme
original est \emph{sleep-lock}.}.
Nous commencerons par les verrous actifs. Xv6 représente
un verrou actif par une \codestyle{struct spinlock}
[\xvline{spinlock.h:/struct.spinlock/}].
Le champ important de cette structure est \codestyle{locked}, un entier
nul lorsque le verrou est libre, et non nul quand le verrou est obtenu.
Logiquement, xv6 obtiendra le verrou en exécutant du code comme :

\code{acquire.c}

Malheureusement, cette implémentation ne garantit pas l'exclusion
mutuelle sur un multiprocesseur.
Il peut arriver que deux processeurs atteignent simultanément la
ligne 25, constatent que \codestyle{lk->locked} est nul, et donc
qu'ils obtiennent tous deux le verrou en exécutant la ligne 26.
À ce stade, deux processeurs différents possèdent le verrou, ce
qui contredit la propriété d'exclusion mutuelle.
Au lieu de nous aider à éviter les conditions de concurrence,
cette implémentation de \codestyle{acquire} exhibe sa propre
condition de concurrence.
Le problème ici est que les lignes 25 et 26 sont exécutées comme
des actions distinctes.
Pour que le code ci-dessus soit correct, les lignes 25 et 26
doivent être exécutées dans une étape \emph{atomique} (c'est-à-dire
indivisible).

Pour exécuter ces deux lignes de manière atomique, xv6 utilise
l'instruction x86 spéciale \xvfct{xchg}{x86.h:/^xchg/}.
En une opération atomique, \codestyle{xchg} échange un mot en mémoire
avec le contenu d'un registre.
La fonction \xvfct{acquire}{spinlock.c:/^acquire/} répète cette
instruction \xvf{xchg} dans une boucle~;
chaque itération lit \codestyle{lk->locked} et y met 1 de manière
atomique \xvline{spinlock.c:/xchg..lk/}.
Si le verrou est déjà obtenu, \codestyle{lk->locked} vaut déjà 1,
donc \xvf{xchg} retournera 1 et la boucle continue.
Si \xvf{xchg} retourne 0, à l'inverse, \xvf{acquire}
a réussi à obtenir le verrou --- \codestyle{locked} valait 0
et vaut maintenant 1 --- donc la boucle peut se terminer.
Une fois le verrou obtenu, pour faciliter le débogage,
\xvf{acquire} enregistre le processeur et la trace des
appels dans la pile qui ont conduit à l'obtention du verrou.
Si un processus oublie de libérer un verrou, cette information
peut aider à identifier le coupable.
Ces champs de débogage sont protégés par le verrou et ne doivent
être modifiés que lorsque le verrou est obtenu.

La fonction \xvfct{release}{spinlock.c:/^release/} est l'opposée
de \xvf{acquire} : elle enlève les informations de débogage, puis
libère le verrou. La fonction utilise une instruction assembleur pour
mettre \codestyle{locked} à 0, car cette remise à 0 doit se faire de
manière atomique pour que l'instruction \xvf{xchg} ne voie pas un
sous-ensemble des 4 octets de \codestyle{locked} mis à jour.
Le x86 garantit qu'une instruction \codestyle{movl} sur 32 bits met à
jour de manière atomique la totalité des 4 octets.
Xv6 ne peut pas utiliser une affectation normale en C, car la
spécification du langage ne précise pas qu'une simple affectation est
atomique.

L'implémentation des verrous actifs dans xv6 est spécifique au x86, et
xv6 est donc non portable vers d'autres processeurs. Pour permettre des
implémentations portables des verrous actifs, le langage C supporte une
bibliothèque d'instructions atomiques~; un système d'exploitation
portable utiliserait ces instructions.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Utiliser les verrous
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Code : Utilisation des verrous}
    \label{sec:utilisation-des-verrous}

Xv6 utilise des verrous à beaucoup d'endroits pour éviter les conditions
de concurrence.
Un exemple simple est dans le pilote IDE.
Comme mentionné au début du chapitre, \xvfct{iderw}{ide.c:/^iderw/}
a une file des requêtes disques et les processeurs peuvent y ajouter en
même temps de nouvelles requêtes [\xvline{ide.c:/DOC:insert-queue/}].
Pour protéger cette file et d'autres invariants dans le pilote,
\xvf{iderw} obtient le verrou \xvfct{idelock}{ide.c:/DOC:acquire-lock/}
et le libère à la fin de la fonction.

L'exercice 1 explore la manière d'exhiber dans le pilote IDE la condition
de concurrence que nous avons vue au début du chapitre en déplaçant
l'appel à \xvf{acquire} après la manipulation de la file. Ça vaut la
peine d'essayer cet exercice car il montre qu'il n'est pas si facile
d'exhiber le problème, ce qui permet de comprendre pourquoi il est
difficile de trouver les bogues dus à des conditions de concurrence.
Il n'est pas certain que xv6 soit exempt de problèmes.

Un point délicat de l'utilisation des verrous est de décider combien il
faut en utiliser et de déterminer quelles données et quels invariants
chaque verrou doit protéger. Il y a quelques principes de base.
Premièrement, à chaque fois qu'une variable peut être modifiée par un
processeur en même temps qu'un autre processeur peut y lire ou y écrire,
un verrou devrait être introduit pour empêcher les deux opérations
de se chevaucher. Deuxièmement, il faut se rappeler que les verrous
protègent des invariants : si un invariant repose sur plusieurs cases
mémoires, toutes celles-là nécessitent typiquement une protection
par un verrou unique pour garantir le maintien de l'invariant.

Les règles ci-dessus permettent de savoir quand les verrous sont
nécessaires, mais ne disent rien sur quand ils ne le sont pas.  Il est
important pour l'efficacité de ne pas trop verrouiller, car les verrous
réduisent le parallélisme. Si le parallélisme n'était pas important,
on pourrait alors n'avoir qu'un seul thread et ne pas s'embêter avec
des verrous. Un noyau simple peut faire ceci sur un multiprocesseur en
ayant un verrou unique qui doit être obtenu à l'entrée dans le noyau, et
libéré lors de la sortie (bien que des appels système comme la lecture
dans un tube ou \codestyle{wait} poseraient des problèmes). Beaucoup de
systèmes d'exploitation monoprocesseurs ont été convertis pour
fonctionner sur des multiprocesseurs en utilisant cette approche,
parfois appelée « verrou géant du noyau », mais elle sacrifie le
parallélisme : seul un processeur peut exécuter le code du noyau
à la fois. Si le noyau fait n'importe quel calcul long, il est plus
efficace d'utiliser un ensemble plus grand de verrous à grain fin, pour
que le noyau puisse être exécuté sur plusieurs processeurs
simultanément.

\begin {figure}[t]
  \begin {center}
    \small
    \begin{tabular}{|l|p{.7\linewidth}|} \hline
      \textbf{Verrou} & \textbf{Description} \\ \hline
      \xvfarg{bcache.lock}{}
	& Protège l'allocation des entrées dans le buffer cache
	\\ \hline
      \xvfarg{cons.lock}{}
	& Sérialise les accès au contrôleur de console, évite les
	    sorties mélangées
	\\ \hline
      \xvfarg{ftable.lock}{}
	& Sérialise l'allocation d'une \codestyle{struct file} dans la
	    table \codestyle{file}
	\\ \hline
      \xvfarg{icache.lock}{}
	& Protège l'allocation des entrées dans le cache des inodes
	\\ \hline
      \xvfarg{idelock}{}
	& Sérialise les accès au contrôleur de disque et à la file
	    des requêtes
	\\ \hline
      \xvfarg{kmem.lock}{}
	& Sérialise l'allocation de mémoire
	\\ \hline
      \xvfarg{log.lock}{}
	& Sérialise les opérations sur le journal des transactions
	\\ \hline
      \xvfarg{p.lock}{} d'un tube
	& Sérialise les opérations sur chaque tube
	\\ \hline
      \xvfarg{ptable.lock}{}
	& Sérialise la commutation de contexte, et les opérations sur
	    \codestyle{proc->state} et la table des processus
	\\ \hline
      \xvfarg{tickslock}{}
	& Sérialise les opérations sur le compteur \codestyle{ticks}
	\\ \hline
      \xvfarg{ip->lock}{}
	& Sérialise les opérations sur chaque inode et son contenu
	\\ \hline
      \xvfarg{b->lock}{}
	& Sérialise les opérations sur chaque buffer
	\\ \hline
    \end{tabular}
  \end {center}
  \caption {Les verrous de xv6.}
  \label{fig:locks}
\end {figure}

Finalement, le choix de la granularité des verrous est un exercice
de programmation parallèle. Xv6 utilise quelques verrous à
gros grain spécifiques pour des structures de données (voir
figure~\ref{fig:locks}). Par exemple, xv6 a un verrou pour protéger la
table entière des processus et ses invariants, qui sont décrits dans le
chapitre~\ref{chap:sched}.
Une approche à grain plus fin serait d'avoir un verrou par entrée dans
la table des processus, afin que des threads travaillant sur des entrées
différentes de la table puissent fonctionner en parallèle.
Cela compliquerait cependant les opérations avec des invariants sur
la table entière, car elles devraient obtenir plusieurs verrous.
Les chapitres suivants aborderont la manière dont chaque partie de xv6
traite la concurrence, illustrant ainsi l'utilisation des verrous.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interblocage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Interblocage et ordre des verrouillages}

Si un chemin dans le code du noyau doit obtenir plusieurs verrous en
même temps, il est important que tous les chemins dans le code
obtiennent ces verrous dans le même ordre. S'ils ne le font pas, il y a
un risque d'interblocage. Supposons que deux chemins dans le code de xv6
nécessitent les verrous A et B, mais le chemin 1 obtient les verrous
dans l'ordre A puis B, et l'autre chemin les obtient dans l'ordre B puis
A. Cette situation peut déboucher sur un interblocage si deux threads
exécutent les deux chemins simultanément. Supposons que le thread T1
exécute le chemin 1 et obtient le verrou A, et que le thread T2 exécute
le chemin 2 et obtient le verrou B. Ensuite, T1 va tenter d'obtenir le
verrou B, et T2 va tenter d'obtenir le verrou A. Les deux requêtes de
verrouillage vont bloquer indéfiniment, car dans les deux cas, l'autre
thread a obtenu le verrou que l'on attend, et ne le libérera pas avant
que son \codestyle{acquire} réussisse. Pour éviter de tels
interblocages, tous les chemins dans le code doivent obtenir les verrous
dans le même ordre. La nécessité d'un ordre global d'acquisition des
verrous signifie que les verrous font effectivement partie intégrante de
la spécification de chaque fonction : les fonctions appelantes doivent
appeler les fonctions de telle manière que les verrous soient obtenus
dans l'ordre convenu.

Xv6 met en œuvre beaucoup de chaînes de verrouillage de longueur 2
impliquant \codestyle{ptable.lock}, dues à la manière dont \xvf{sleep}
fonctionne comme évoqué dans le chapitre~\ref{chap:sched}. Par
exemple, \codestyle{ideintr} verrouille \codestyle{idelock} et appelle
\xvf{wakeup}, qui obtient le verrou \codestyle{ptable.lock}. Le code du
système de fichiers abrite les plus longues chaînes de verrouillage
de xv6. Par exemple, créer un fichier nécessite le verrouillage du
répertoire, de l'inode du nouveau fichier, du buffer contenant le bloc
disque, de \codestyle{idelock} et de \codestyle{ptable.lock}. Pour éviter
un interblocage, le code du système de fichiers obtient toujours les
verrous dans l'ordre mentionné dans la précédente phrase.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gestionnaires d'interruption
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Gestionnaires d'interruption}

Xv6 utilise des verrous actifs dans de nombreuses situations pour
protéger des données manipulées à la fois par des gestionnaires
d'interruption et par des threads. Par exemple, une interruption
d'horloge pourrait [\xvline {trap.c:/T_IRQ0...IRQ_TIMER/}] incrémenter
\codestyle{ticks} en même temps qu'un thread du noyau pourrait consulter
cette variable dans \xvfct{sys\_sleep}{sysproc.c:/ticks0.=.ticks/}. Le
verrou \codestyle{tickslock} sérialise les deux accès.

Les interruptions peuvent être une source de concurrence même sur un
monoprocesseur : si les interruptions ne sont pas ignorées, le code
du noyau peut être interrompu à tout moment pour laisser la place à
un gestionnaire d'interruption. Supposons que la fonction \xvf{iderw}
ait obtenu le verrou \codestyle{idelock} et soit ensuite interrompue
pour exécuter \xvf{ideintr} ; celle-ci essayera alors de verrouiller
\codestyle{idelock}, constatera qu'il est déjà verrouillé, et attendra
sa libération. Dans ce cas, \codestyle{idelock} ne sera jamais libéré
: seule \xvf{iderw} peut le libérer et elle ne reprendra pas avant que
\xvf{ideintr} ne se termine. Le processeur, et à la fin le système
entier, sera interbloqué.

Pour éviter cette situation, si un verrou actif est utilisé par un
gestionnaire d'interruption, il ne faut pas qu'un processeur conserve
ce verrou lorsque les interruptions sont autorisées.
Xv6 est plus conservateur : quand un processeur entre dans une section
critique contrôlée par un verrou actif, le noyau garantit que les
interruptions sont ignorées sur ce processeur. Les interruptions
peuvent toujours être prises en compte sur les autres processeurs,
donc un appel à \xvf{acquire} dans un gestionnaire d'interruption peut
provoquer une attente, qui sera satisfaite par le déverrouillage dans
un thread du noyau ; il ne faut juste pas que ce soit sur le même
processeur.

Xv6 autorise à nouveau les interruptions quand un processeur n'a plus
aucun verrou actif verrouillé~; il faut faire un peu de comptabilité
pour gérer les sections critiques imbriquées. La fonction \xvf{acquire}
appelle \xvfct{pushcli}{spinlock.c:/^pushcli/} et \xvf{release} appelle
\xvfct{popcli}{spinlock.c:/^popcli/} pour compter le niveau d'imbrication
des verrous sur le processeur courant. Quand ce décompte devient nul,
\xvf{popcli} restaure l'état d'autorisation des interruptions en
vigueur avant le début de la section critique la plus englobante. La
fonction \xvf{cli} (resp. \xvf{sti}) exécute l'instruction du x86 pour
ignorer (resp. activer) les interruptions.

Il faut que \xvf{acquire} appelle \xvf{pushcli} avant \xvf{xchg}
[\xvline{spinlock.c:/while.xchg/}].  Si les deux appels étaient
inversés, le verrou pourrait être obtenu pendant quelques cycles alors
que les interruptions sont encore autorisées, et une interruption
malencontreuse au mauvais moment pourrait alors interbloquer le
système. Symétriquement, il faut que \xvf{release} appelle \xvf{popcli}
seulement après le \codestyle{movl} qui libère le verrou.

% NOTE : il me semble qu'il y a un bug dans l'original : release appelle
% movl et non xchg

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instructions et ordonnancement des accès à la mémoire
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Instructions et ordonnancement des accès à la mémoire}

Dans ce chapitre, nous avons supposé que le code est exécuté dans
l'ordre dans lequel il apparaît dans le programme. Cependant, nombre
de compilateurs et de processeurs exécutent le code sans respecter
l'ordre du programme pour atteindre de meilleures performances.
Si une instruction met plusieurs cycles, un processeur peut vouloir
démarrer plus tôt cette instruction de telle manière qu'elle s'exécute
en même temps que d'autres instructions, et ainsi éviter que le
processeur ne soit retardé.
Par exemple, un processeur peut détecter que dans une séquence
d'instructions A puis B, les deux instructions ne sont pas dépendantes
l'une de l'autre, et qu'il peut démarrer B avant A de telle manière que
B soit terminée quand A se terminera. Un compilateur peut effectuer un
réordonnancement similaire en générant l'instruction B avant
l'instruction A dans le fichier exécutable.
Cependant, la concurrence peut rendre visible ce réordonnancement au
niveau logiciel, ce qui peut conduire à un comportement incorrect.

Par exemple, dans le code ci-après pour \codestyle{insert}, ce serait
désastreux si le compilateur ou le processeur rendait visible par les
autres processeurs l'effet de la ligne 4 (ou 2 ou 5) après l'effet de la
ligne 6~:

\code{reorder.c}

Si le matériel ou le compilateur réordonnait, par exemple, les effets
de la ligne 4 et les rendait visibles avant les effets de la ligne 6,
alors un autre processeur pourrait verrouiller \codestyle{listlock}
et observer que \codestyle{list} pointe sur \codestyle{l}, mais il
n'observerait pas que \codestyle{l->next} pointe sur le reste de la
liste et ne pourrait donc pas voir le reste de la liste.

Pour indiquer au matériel et au compilateur de ne pas effectuer de tels
réordonnancements, xv6 utilise \codestyle{\_\_sync\_synchronize()} à la
fois dans \xvf{acquire} et dans \xvf{release}.
La fonction \codestyle{\_\_sync\_synchronize} est une barrière
mémoire~: elle indique au compilateur et au processeur de ne pas
réordonnancer les accès à la mémoire à travers la barrière.
Xv6 ne se préoccupe du réordonnancement que dans \xvf{acquire} et
\xvf{release} car les accès concurrents aux structures de données
autres que la structure du verrou sont effectués entre \xvf{acquire}
et \xvf{release}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verrous passifs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Verrous passifs}

Le code de xv6 doit parfois conserver un verrou pendant une longue
durée. Par exemple, le système de fichiers (voir chapitre~\ref{chap:fs})
maintient le fichier verrouillé pendant la lecture ou l'écriture de son
contenu sur le disque, ces opérations pouvant prendre
des dizaines de millisecondes.
L'efficacité impose de libérer le processeur pendant l'attente afin
que d'autres threads puissent progresser, ceci signifie donc que 
xv6 utilise des verrous devant fonctionner correctement lorsqu'ils
sont conservés entre plusieurs commutations de contexte.
Xv6 fournit de tels verrous sous la forme de \emph{verrous passifs}.

Les verrous passifs de xv6 supportent le fait de libérer le
processeur à l'intérieur de la section critique. Cette propriété
constitue une difficulté de conception : si le thread T1 a obtenu le verrou
L1 et a libéré le processeur, et que le thread T2 souhaite obtenir
L1, nous devons garantir que T1 doit pouvoir s'exécuter pendant que
T2 attend, afin que T1 puisse déverrouiller L1. T2 ne peut pas
utiliser la fonction \xvf{acquire} sur un verrou actif ici : elle boucle
en ignorant les interruptions, empêchant ainsi T1 de s'exécuter.
Pour éviter cet interblocage, la fonction pour obtenir un verrou passif
(appelée \xvf{acquiresleep}) libère le processeur pendant l'attente
et ne désactive pas les interruptions.

La fonction \xvfct{acquiresleep}{sleeplock.c:/^acquiresleep/} utilise
des techniques qui seront expliquées dans le chapitre~\ref{chap:sched}.
Vu de haut, un verrou passif possède un champ \codestyle{locked}
lui-même protégé par un verrou actif, et l'appel à \xvf{sleep} par
\xvf{acquiresleep} réalise de manière atomique la libération du
processeur et le déverrouillage du verrou actif.
Le résultat est que les autres threads peuvent s'exécuter pendant
que \xvf{acquiresleep} attend.

Du fait que les verrous passifs laissent les interruptions autorisées,
ils ne peuvent pas être utilisés dans les gestionnaires d'interruption.
Puisque \xvf{acquiresleep} peut libérer le processeur, les verrous
passifs ne peuvent pas non plus être utilisés dans des sections
critiques protégées par des verrous actifs (bien que l'inverse soit
vrai : les verrous actifs peuvent être utilisés dans des sections
critiques protégées par des verrous passifs).

La plupart du temps, xv6 utilise les verrous actifs car ils ont un faible
\emph{overhead}\ndtshort{Nous ne traduisons pas ce terme, largement
utilisé et n'ayant pas de traduction établie en Français.}. Xv6
utilise les verrous passifs uniquement dans le système de fichiers, où
il est pratique de pouvoir conserver des verrous pendant des opérations
lentes sur les disques.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Limitations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Limitations des verrous}

Les verrous permettent souvent de résoudre proprement les problèmes de
concurrence, mais il y a des cas où ils sont gênants.
Les chapitres suivants pointeront de telles situations dans xv6~;
cette section expose les grandes lignes des problèmes qui se
présenteront.

Il arrive parfois qu'une fonction utilise des données devant être
protégées par un verrou, et que cette fonction soit appelée à la fois
par du code qui a déjà obtenu ce verrou et par du code qui n'aurait
autrement pas besoin de ce verrou. Une manière de gérer ce cas est
d'avoir deux variantes de la fonction, une qui obtient explicitement
le verrou et une autre qui suppose que l'appelant l'a déjà obtenu.
Voir \xvfct{wakeup1}{proc.c:/^wakeup1/} par exemple.
Une autre approche consiste pour la fonction à avoir comme prérequis que
les fonctions appelantes obtiennent le verrou, qu'elles en aient
l'utilité ou non, comme avec \xvfct{sched}{proc.c:/^sched.void/}.
Les développeurs du noyau doivent avoir conscience de tels prérequis.

Il pourrait sembler possible de simplifier les cas où à la fois
l'appelante et l'appelée ont besoin d'un verrou en permettant les
\emph{verrous récursifs} : si une fonction a déjà obtenu un verrou,
toute fonction qu'elle appelle est autorisée à obtenir à nouveau ce
verrou. Cependant, le programmeur devra alors réfléchir à toutes
les combinaisons de l'appelante et de l'appelée, car les invariants
de la structure de données ne seront pas forcément vérifiés après
l'obtention du verrou.
La question de savoir si les verrous récursifs sont mieux que
l'utilisation par xv6 des conventions sur les fonctions ayant comme
prérequis l'obtention d'un verrou n'est pas claire.
La plus grande leçon est, comme avec l'ordre global de verrouillage pour
éviter les interblocages, que les prérequis sur les verrous ne peuvent
pas être privés mais doivent être inclus dans les interfaces des
fonctions et des modules.

Un cas où les verrous sont insuffisants est lorsqu'un thread doit
attendre qu'un autre thread mette à jour une structure de données, par
exemple quand un lecteur dans un tube attend qu'un autre thread écrive
dans le tube. Le thread en attente ne peut pas conserver le verrou sur
les données, car cela empêcherait la mise à jour que ce thread attend.
À la place, xv6 fournit un mécanisme distinct qui gère à la fois le
verrou et l'attente de l'événement~; voir la description de \xvf{sleep}
et \xvf{wakeup} dans le chapitre~\ref{chap:sched}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Le monde réel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Le monde réel}

Les primitives relatives à la concurrence et la programmation parallèle
sont des domaines de recherche actifs, car la programmation avec les
verrous représente toujours un défi.
Il est préférable d'utiliser les verrous comme une base pour des
constructions de plus haut niveau, telles que les files synchronisées,
bien que xv6 ne fasse pas cela.
Si vous programmez avec des verrous, il est prudent d'utiliser un outil
qui tente d'identifier les conditions de concurrence car il est facile
de rater un invariant nécessitant un verrou.

La plupart des systèmes d'exploitation supportent les threads POSIX
(pthreads), qui permettent à un processus utilisateur d'avoir plusieurs
threads s'exécutant simultanément sur différents processeurs. Les
pthreads supportent les verrous en mode utilisateur, les barrières, etc.
Supporter les pthreads nécessite du support du système d'exploitation.
Par exemple, si un pthread attend dans un appel système, un autre
pthread du même processus devrait pouvoir s'exécuter sur le processeur.
Un autre exemple est lorsqu'un pthread change l'espace d'adressage du
processus (en l'élargissant ou le rétrécissant), le noyau doit alors
faire en sorte que les autres processeurs exécutant des threads du même
processus mettent à jour leur table des pages pour refléter cette
modification.
Sur le x86, ceci implique de vider le \emph{Translation Look-aside Buffer}
des autres processeurs en utilisant des interruptions
inter-processeurs\ndtshort{Le terme original est \emph{Inter-processor
interrupts}, ou IPI.}

Il est possible d'implémenter les verrous sans utiliser des instructions
atomiques, mais c'est coûteux et la plupart des systèmes d'exploitation
utilisent les instructions atomiques.

Les verrous peuvent être coûteux si beaucoup de processeurs tentent
d'obtenir le même verrou au même moment. Si un processeur a un verrou
dans son cache local et un autre processeur veut obtenir ce verrou,
alors l'instruction atomique qui met à jour la ligne de cache qui
contient le verrou doit propager la ligne de cache vers les autres
processeurs, et également invalider les autres copies de cette ligne
de cache. Récupérer une ligne de cache d'un autre processeur peut être
plus coûteux de plusieurs ordres de grandeur que récupérer une ligne
du cache local du processeur.

Pour éviter les coûts associés aux verrous, beaucoup de systèmes
d'exploitation utilisent des structures de données et des algorithmes
\emph{sans verrouillage}\ndtshort{Le terme original est
\emph{lock-free}.}. Par exemple, il est possible d'implémenter une liste
chaînée comme celle du début du chapitre sans avoir besoin de verrou et
en utilisant juste une instruction atomique pour insérer un élément.
Cependant, la programmation sans verrouillage est plus complexe
qu'avec des verrous~; par exemple, on doit être conscient du
réordonnancement des instructions et des accès mémoire.
Programmer avec des verrous est déjà difficile, xv6 évite donc la
complexité supplémentaire de la programmation sans verrouillage.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Exercices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section {Exercices}

\begin {enumerate}
    \item Déplacez l'appel à \xvf{acquire} dans \xvf{idrw} avant
	l'appel à \xvf{sleep}. Est-ce qu'il y a une condition de
	concurrence ?
	Pourquoi ne l'observez-vous pas lorsque vous démarrez xv6 et
	lancez \codestyle{stressfs}~?
	Ajoutez une boucle vide dans la section critique~; que
	constatez-vous maintenant~?
	Expliquez.

    \item Retirez l'appel à \xvf{xchg} dans \xvf{acquire}. Expliquez
	ce qui se passe lorsque vous démarrez xv6.

    \item Écrivez un programme parallèle utilisant les threads POSIX,
	qui sont supportés sur la plupart des systèmes d'exploitation.
	Par exemple, implémentez une table de hachage parallèle et
	vérifiez par la mesure si le nombre d'insertions et de recherches
	est proportionnel au nombre de cœurs.

    \item Implémentez un sous-ensemble des pthreads dans xv6, c'est-à-dire
	implémentez une bibliothèque en mode utilisateur pour qu'un
	processus puisse avoir plus d'un thread et faites en sorte que
	ces threads s'exécutent en parallèle sur des processeurs
	distincts. Votre conception doit gérer correctement le cas
	où un thread fait un appel bloquant au système et le cas où un
	thread change l'espace d'adressage partagé.

\end {enumerate}
